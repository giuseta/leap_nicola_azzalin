/**
 * This file is part of LEAP.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * LEAP is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * LEAP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package unife.edge;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import mpi.MPI;
import mpi.MPIException;
import mpi.Status;
import net.sf.javabdd.BDD;
import org.apache.log4j.Logger;
import org.mindswap.pellet.utils.Timers;
import org.semanticweb.owlapi.model.OWLAxiom;
import unife.bundle.logging.BundleLoggerFactory;
import unife.bundle.utilities.BundleUtilities;
import static unife.edge.mpi.EDGEMPIConstants.ALL;
import static unife.edge.mpi.EDGEMPIConstants.ALL_BCAST;
import static unife.edge.mpi.EDGEMPIConstants.CONTINUE;
import static unife.edge.mpi.EDGEMPIConstants.STOP;
import unife.edge.mpi.MPIUtilities;
import unife.edge.mpi.RecvObjectStatus;

/**
 *
 * @author Giuseppe Cota <giuseta@gmail.com>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class EDGEMPISingleStep extends EDGE {

    private enum Type {
        POSITIVE,
        NEGATIVE
    }
    private final boolean MASTER;
    private int myRank;
    private static Logger logger;
    private final List<Map.Entry<OWLAxiom, Type>> initialExamples = new ArrayList<>(); // all the starting examples
    private int tagFirstNegative = 0;

    private List<List<Map.Entry<OWLAxiom, Type>>> examplesDistribution;

    public EDGEMPISingleStep() {
        try {
            myRank = MPI.COMM_WORLD.getRank();
            MASTER = MPIUtilities.isMaster(MPI.COMM_WORLD);
            logger = Logger.getLogger(EDGEMPISingleStep.class.getName(),
                    new BundleLoggerFactory());
        } catch (MPIException mpiEx) {
            String msg = "Impossible to get the Rank: " + mpiEx.getMessage();
            System.err.println(msg);
            logger.error(msg);
            throw new RuntimeException(msg);
        }
    }

//    @Override
//    public void init() {
//        super.init();
//        if (MASTER) {
//            initialExamples.addAll(getExamples());
//        }
//    }
    @Override
    protected void printTestResult(Timers timers, BigDecimal[] probs) {
        if (MASTER) {
            logger.info("");
            logger.info("============ Result ============");
            logger.info("");
            int index = 0;
            for (Map.Entry<OWLAxiom, Type> query : initialExamples) {
                logger.info("Query " + (index + 1) + " of " + initialExamples.size()
                        + " (" + (int) (((index + 1) * 100) / initialExamples.size()) + "%)");
                if (query.getValue() == Type.POSITIVE) {
                    logger.info("Positive Example: " + query.getKey());
                } else {
                    logger.info("Negative Example: " + query.getKey());
                }
                logger.info("Prob: " + probs[index]);
                index++;
            }
            logger.info("");
            logger.info("================================");
            logger.info("");
            StringWriter sw = new StringWriter();
            timers.print(sw, true, null);
            logger.info(sw);
        } else { // SLAVE
            super.printTestResult(timers, probs);
        }
    }

    @Override
    public Map<OWLAxiom, BigDecimal> preparePMap() {
        Map<OWLAxiom, BigDecimal> PMap;
        if (MASTER) {
            PMap = super.preparePMap();
            if (PMap == null) {
                logger.warn("I don't have probabilistic axioms in PMap! I have nothing to learn!");
                throw new RuntimeException("Nothing to do! See log for details...");
            }
            sendPMap(PMap);
        } else {
            PMap = receivePMap();
        }
        return PMap;
    }

    private void sendPMap(Map<OWLAxiom, BigDecimal> PMap) {

        try {

            byte[] bufferRulesName, bufferProbs, bufferSend;
            bufferRulesName = MPIUtilities.objectToByteArray(rulesName);
            List<BigDecimal> probabilitiesOfPMap = new ArrayList<>(rulesName.size());
            for (OWLAxiom ax : rulesName) {
                probabilitiesOfPMap.add(PMap.get(ax));
            }
            bufferProbs = MPIUtilities.objectToByteArray(probabilitiesOfPMap);

            int[] bufferDim = {bufferRulesName.length, bufferProbs.length};
            bufferSend = new byte[bufferRulesName.length + bufferProbs.length];
            System.arraycopy(bufferRulesName, 0, bufferSend, 0, bufferRulesName.length);
            System.arraycopy(bufferProbs, 0, bufferSend, bufferRulesName.length, bufferProbs.length);
            logger.debug(myRank + " - Packing PMap...");
            byte[] bufferPack = new byte[bufferSend.length + (Integer.SIZE / 8)];
            int pos = MPI.COMM_WORLD.pack(bufferDim, 1, MPI.INT, bufferPack, 0);
            MPI.COMM_WORLD.pack(bufferSend, bufferSend.length, MPI.BYTE, bufferPack, pos);

            logger.debug(myRank + " - Sending PMap...");
            //MPI.COMM_WORLD.send(bufferDim, 2, MPI.INT, i, 0);
            //logger.debug(myRank + " - Sent: [ " + bufferDim[0] + " " + bufferDim[1] + " ]");
            //MPI.COMM_WORLD.send(bufferSend, bufferSend.length, MPI.BYTE, i, 0);
            // NOTE: BROADCAST IMPLIES 2 SENDS AT THIS MOMENT BECAUSE FUNCTION PROBE DOESN'T
            // WORK WITH BCAST AND SLAVES NEED TO KNOW HOW MANY BYTE TO RECEIVE
            for (int i = 1; i <= MPIUtilities.getSlaves(MPI.COMM_WORLD); i++) {
                MPI.COMM_WORLD.send(bufferPack, bufferPack.length, MPI.PACKED, i, 0);
                logger.debug(myRank + " - Sent " + bufferSend.length + " bytes...");
            }
        } catch (Exception ex) {
            logger.error(myRank + " - Error: " + ex);
            throw new RuntimeException(ex.getMessage());
        }
    }

    private Map<OWLAxiom, BigDecimal> receivePMap() {
        Map<OWLAxiom, BigDecimal> PMap = null;
        try {
            int[] bufferDim = new int[2];
            logger.debug(myRank + " - Receiving PMap...");
            Status status = MPI.COMM_WORLD.probe(0, 0);
            byte[] bufferRecvTot = new byte[status.getCount(MPI.PACKED)];
            logger.debug(myRank + " - Receiving " + bufferRecvTot.length + " bytes...");
            MPI.COMM_WORLD.recv(bufferRecvTot, bufferRecvTot.length, MPI.PACKED, 0, 0);
            logger.debug(myRank + " - Receiving bytes OK...");
            int pos = MPI.COMM_WORLD.unpack(bufferRecvTot, 0, bufferDim, 1, MPI.INT);
            bufferDim[1] = (bufferRecvTot.length - pos - bufferDim[0]);
            byte[] bufferRecv = new byte[bufferDim[0] + bufferDim[1]];
            MPI.COMM_WORLD.unpack(bufferRecvTot, pos, bufferRecv, bufferRecv.length, MPI.BYTE);
            byte[] bufferRulesName = new byte[bufferDim[0]];
            byte[] bufferProbs = new byte[bufferDim[1]];
            System.arraycopy(bufferRecv, 0, bufferRulesName, 0, bufferDim[0]);
            System.arraycopy(bufferRecv, bufferDim[0], bufferProbs, 0, bufferDim[1]);

            // NOTA in realtà rulesName non servirebbe allo slave (in questa versione serve perché usato nell'init)
            //      nella versione Parallelization rulesName non esiste negli slaves
            rulesName = (List<OWLAxiom>) MPIUtilities.byteArrayToObject(bufferRulesName);
            logger.debug(myRank + " - Received: " + rulesName);
            List<BigDecimal> obj = (List<BigDecimal>) MPIUtilities.byteArrayToObject(bufferProbs);

            PMap = new HashMap<>();
            rulesNameString = new ArrayList<>();
            arrayProb = new ArrayList<>();
            String array = "";
            for (int i = 0; i < rulesName.size(); i++) {
                OWLAxiom ax = rulesName.get(i);
                rulesNameString.add(BundleUtilities.getManchesterSyntaxString(ax));
                PMap.put(ax, obj.get(i));
                arrayProb.add(obj.get(i));
                array += "" + ax + " => " + arrayProb.get(i);
            }
            logger.debug(myRank + " - Received: " + obj);
            logger.debug(myRank + " - [" + array + " ]");
        } catch (Exception ex) {
            logger.error("Some problem in receiving PMap: " + ex);
            throw new RuntimeException(ex);
        }
        return PMap;

    }

    @Override
    public BigDecimal[] computeExamples(Map<OWLAxiom, BigDecimal> PMap, 
            List<BDD> BDDs, boolean test) {
        if (MASTER) {
            // populate initialExamples list
            for (OWLAxiom pEx : getPositiveExamples()) {
                initialExamples.add(new AbstractMap.SimpleEntry<>(pEx, Type.POSITIVE));
            }
            for (OWLAxiom nEx : getNegativeExamples()) {
                initialExamples.add(new AbstractMap.SimpleEntry<>(nEx, Type.NEGATIVE));
            }

            logger.info(myRank + " - Start finding explanations for every example...");
            // Invio esempi in blocchi
            logger.info(myRank + " - Sending examples...");
            sendExamplesThread();
            // compute examples (The master is a slave of himself)
            List<OWLAxiom> masterPositiveExamples = new ArrayList<>();
            List<OWLAxiom> masterNegativeExamples = new ArrayList<>();
            for (Entry<OWLAxiom, Type> example : examplesDistribution.get(0)) {
                if (example.getValue() == Type.POSITIVE) {
                    masterPositiveExamples.add(example.getKey());
                } else {
                    masterNegativeExamples.add(example.getKey());
                }
            }
            setPositiveExamples(masterPositiveExamples);
            setNegativeExamples(masterNegativeExamples);
            int sum = getExamples().size();
            for (int i = 1; i < examplesDistribution.size(); i++) {
                sum += examplesDistribution.get(i).size();
            }
            if (sum != initialExamples.size()) {
                logger.error("Problem in examples: number of examples mismatch ( " + sum + " / " + initialExamples.size() + " )");
                throw new RuntimeException("Problem in examples: number of examples mismatch ( " + sum + " / " + initialExamples.size() + " )");
            }
            
            //double masterProbs[] = new double[examplesDistribution.get(0).size()];
            BigDecimal masterProbs[] = super.computeExamples(PMap, BDDs, test);
            for (int i = 0; i < masterProbs.length; i++) {
                logger.debug(myRank 
                        + " - " + BundleUtilities.getManchesterSyntaxString(initialExamples.get(i).getKey())
                        + " - prob: " + masterProbs[i] + " - tag: " + 
                        (i+1) + " - #vars: " + boolVars_ex.get(i));
            }
            // ricevo i risultati
            BigDecimal[] probs = new BigDecimal[initialExamples.size()];
            receiveExamplesResults(probs, test);
            System.arraycopy(masterProbs, 0, probs, 0, masterProbs.length);
            return probs;
        } else { // SLAVE
            logger.info(myRank + " - Start finding explanations for every example...");
            // receive the queries from the master
            int[] queryTags = receiveExamples();
            logger.info(myRank + " - Example received...");
            BigDecimal[] probs = super.computeExamples(PMap, BDDs, test);
            // send probabilities of the queries to the master
            sendExamplesResults(queryTags, probs, test);
            return probs;
        }
    }

    private void sendExamplesThread() {
        int numSlaves = 0;
        try {
            numSlaves = MPIUtilities.getSlaves(MPI.COMM_WORLD);
            logger.debug(myRank + " - I have " + numSlaves + " slaves..");
        } catch (MPIException ex) {
            logger.error(myRank + " - Error: " + ex);
            System.exit(-1);
        }
        logger.debug(myRank + " - I have " + initialExamples.size() + " examples");
        examplesDistribution = new ArrayList<>(numSlaves + 1);
        int chunkDim = initialExamples.size() / (numSlaves + 1);

        List<OWLAxiom> positiveExamples = getPositiveExamples();
        List<OWLAxiom> negativeExamples = getNegativeExamples();

        int numExtraLastSlaveExamples = initialExamples.size() % (numSlaves + 1);
        int pos = 0;
//        List<Map.Entry<OWLAxiom, Type>> masterMapExamples = new ArrayList<>();
//        for (int j = 0; j < numMasterExamples; j++) {
//            if (pos < positiveExamples.size()) {
//                masterMapExamples.add(new AbstractMap.SimpleEntry<>(positiveExamples.get(pos), Type.POSITIVE));
//            } else {
//                masterMapExamples.add(new AbstractMap.SimpleEntry<>(negativeExamples.get(pos - positiveExamples.size()), Type.NEGATIVE));
//            }
//            pos++;
//        }
//        examplesDistribution.add(masterMapExamples);

        for (int i = 0; i < numSlaves + 1; i++) {
            List<Map.Entry<OWLAxiom, Type>> nodeMapExamples = new ArrayList<>();
            for (int j = 0; j < chunkDim; j++) {
                if (pos < positiveExamples.size()) {
                    nodeMapExamples.add(new AbstractMap.SimpleEntry<>(positiveExamples.get(pos), Type.POSITIVE));
                } else {
                    nodeMapExamples.add(new AbstractMap.SimpleEntry<>(negativeExamples.get(pos - positiveExamples.size()), Type.NEGATIVE));
                }
                pos++;
            }
            if (i == numSlaves) {
                for (int j = 0; j < numExtraLastSlaveExamples; j++) {
                    if (pos < positiveExamples.size()) {
                        nodeMapExamples.add(new AbstractMap.SimpleEntry<>(positiveExamples.get(pos), Type.POSITIVE));
                    } else {
                        nodeMapExamples.add(new AbstractMap.SimpleEntry<>(negativeExamples.get(pos - positiveExamples.size()), Type.NEGATIVE));
                    }
                    pos++;
                }
            }
            examplesDistribution.add(nodeMapExamples);
        }
        logger.debug(myRank + " - setting up for " + (numSlaves + 1) + " chunk dimensions...");
        String array = "";
        for (int i = 0; i < examplesDistribution.size(); i++) {
            array += " " + examplesDistribution.get(i).size() + " ";
        }
        logger.info(myRank + " - [" + array + "]");

//        for (int i = 0; i < examplesDistribution.get(0).size(); i++) {
//            vars_ex.add(new ArrayList<Integer>());
//            probs_ex.add(new ArrayList<Double>());
//            boolVars_ex.add(0);
//        }
        //ExamplesSender[] sender = new ExamplesSender[getSlaves()];
        for (int j = 1; j <= numSlaves; j++) {
            (new Thread(new EDGEMPISingleStep.ExamplesSender(j))).start();
        }
    }

    private void receiveExamplesResults(BigDecimal[] probs, boolean test) {

        int received = examplesDistribution.get(0).size();
        while (received < initialExamples.size()) {
            try {

                Status recvStat = MPI.COMM_WORLD.probe(MPI.ANY_SOURCE, MPI.ANY_TAG);
                byte[] buffer = new byte[recvStat.getCount(MPI.PACKED)];
                int[] intBuffer = new int[1];

                MPI.COMM_WORLD.recv(buffer, buffer.length, MPI.PACKED, recvStat.getSource(), recvStat.getTag());
                // Probability
                int pos = MPI.COMM_WORLD.unpack(buffer, 0, intBuffer, 1, MPI.INT);
                byte[] bytesOfProb = new byte[intBuffer[0]];
                pos = MPI.COMM_WORLD.unpack(buffer, pos, bytesOfProb, bytesOfProb.length, MPI.BYTE);
                int index = recvStat.getTag() - 1;
                probs[index] = (BigDecimal)MPIUtilities.byteArrayToObject(bytesOfProb);
                String msg = myRank + " - RECV from " + recvStat.getSource() 
                        + " - " + BundleUtilities.getManchesterSyntaxString(initialExamples.get(index).getKey())
                        + " - prob: " + probs[index] + " - tag: " + recvStat.getTag();
                
                if (!test) {
                    intBuffer = new int[1];
                    pos = MPI.COMM_WORLD.unpack(buffer, pos, intBuffer, 1, MPI.INT);
                    msg += " - #vars: " + intBuffer[0];
                    logger.info(msg);
                    intBuffer = new int[intBuffer[0]];
//                doubleBuffer = new double[intBuffer.length];
                    if (intBuffer.length > 0) {
//                    buffer = new byte[(Double.SIZE/8 + Integer.SIZE/8) * intBuffer.length];
                        //buffer = new byte[(Integer.SIZE / 8) * intBuffer.length];
                        //MPI.COMM_WORLD.recv(buffer, buffer.length, MPI.PACKED, recvStat.getSource(), recvStat.getTag());

                        MPI.COMM_WORLD.unpack(buffer, pos, intBuffer, intBuffer.length, MPI.INT);
//                    MPI.COMM_WORLD.unpack(buffer, pos, doubleBuffer, doubleBuffer.length, MPI.DOUBLE);
                    }

//                List<Integer> to_add_vars_ex = new ArrayList<>();
//                List<Double> to_add_probs_ex = new ArrayList<>();
                    logger.info("intBuffer");
                    for (int i = 0; i < intBuffer.length; i++) {
                        logger.info(intBuffer[i]);
//                    to_add_vars_ex.add(intBuffer[i]);
//                    to_add_probs_ex.add(doubleBuffer[i]);
                        usedAxioms[intBuffer[i]] = true;
                        //logger.debug(myRank + " - var: " + intBuffer[i] + " prob: " + doubleBuffer[i]);
                    }
                }
//                vars_ex.set(tag, to_add_vars_ex);
//                probs_ex.set(tag, to_add_probs_ex);
//                boolVars_ex.set(tag, intBuffer.length);
//                logger.debug(myRank + " - tag " + tag + " - " + probs_ex.get(tag) + " - " + vars_ex.get(tag));
                logger.debug(msg);
                received++;

            } catch (MPIException ex) {
                // provo a reinviare??
                logger.error(myRank + " - Error: " + ex);
            }
        }
    }

    private int[] receiveExamples() {
        int[] dim = new int[2];
        try {
            logger.debug(myRank + " - Receiving examples...");
            MPI.COMM_WORLD.recv(dim, 2, MPI.INT, 0, 0);
            int[] queryTags = new int[dim[0]];
            logger.debug(myRank + " - I have " + dim[0] + " examples (" + dim[1] + " positives)...");

            OWLAxiom query;
            int numPosEx = dim[1];
            List<OWLAxiom> slavePositiveExamples = new ArrayList<>(numPosEx);
            List<OWLAxiom> slaveNegativeExamples = new ArrayList<>(dim[0] - numPosEx);
            for (int i = 0; i < dim[0]; i++) {
                logger.debug(myRank + " - Receiving " + (i + 1) + "-th example...");
                RecvObjectStatus stat = MPIUtilities.recvObject(0, MPI.ANY_TAG, MPI.COMM_WORLD);
                query = (OWLAxiom) stat.getObj();
                logger.debug(myRank + " - " + query);
                if (i < numPosEx) {
                    slavePositiveExamples.add(query);
                } else {
                    slaveNegativeExamples.add(query);
                }
                logger.debug(myRank + " - " + stat.getStatus().getTag() + ": " + query);
                queryTags[i] = stat.getStatus().getTag();
//                vars_ex.add(new ArrayList<Integer>());
//                probs_ex.add(new ArrayList<Double>());
//                boolVars_ex.add(0);
                //MPIUtilities.sendSignal(0, MPIUtilities.CONTINUE);
            }
            setPositiveExamples(slavePositiveExamples);
            setNegativeExamples(slaveNegativeExamples);
            return queryTags;
        } catch (MPIException ex) {
            logger.error(myRank + "Error: " + ex.getMessage());
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            logger.error(myRank + "Error: " + ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    private void sendExamplesResults(int[] queryTags, BigDecimal[] probs, boolean test) {

        for (int i = 0; i < getExamples().size(); i++) {
            try {

                byte[] probArray = MPIUtilities.objectToByteArray(probs[i]);
                int[] intBuffer = {probArray.length};
                byte[] buffer = new byte[Integer.SIZE / 8 + intBuffer[0] + 
                        (!test ? ((Integer.SIZE / 8)  * (vars_ex.get(i).size() + 1)) : 0)];
                        
                

                int pos = MPI.COMM_WORLD.pack(intBuffer, 1, MPI.INT, buffer, 0);
                pos = MPI.COMM_WORLD.pack(probArray, probArray.length, MPI.BYTE, buffer, pos);
                if (!test) {
                    intBuffer[0] = vars_ex.get(i).size();
                    pos = MPI.COMM_WORLD.pack(intBuffer, 1, MPI.INT, buffer, pos);
                    logger.debug(myRank + " - i " + i + " - " + probs[i] + " - " + vars_ex.get(i).size() + " - tag: " + queryTags[i]);
                    //MPI.COMM_WORLD.send(buffer, buffer.length, MPI.PACKED, 0, queryTags[i]);
                    logger.debug(myRank + " - i " + i + " - " + probs_ex.get(i) + " - " + vars_ex.get(i));

                    if (intBuffer[0] > 0) {
                        logger.debug(myRank + " - n of vars: " + intBuffer[0] + " - Sending...");
//                    buffer = new byte[(Double.SIZE / 8 + Integer.SIZE / 8) * intBuffer[0]];
                        intBuffer = new int[intBuffer[0]];
//                    doubleBuffer = new double[intBuffer.length];
                        logger.info("intBuffer");
                        for (int j = 0; j < vars_ex.get(i).size(); j++) {
                            intBuffer[j] = vars_ex.get(i).get(j);
//                        doubleBuffer[j] = probs_ex.get(i).get(j);
                            logger.info(intBuffer[j]);
                        }
                        MPI.COMM_WORLD.pack(intBuffer, intBuffer.length, MPI.INT, buffer, pos);
//                    MPI.COMM_WORLD.pack(doubleBuffer, doubleBuffer.length, MPI.DOUBLE, buffer, pos);
                    }
                }
                MPI.COMM_WORLD.send(buffer, buffer.length, MPI.PACKED, 0, queryTags[i]);
            } catch (MPIException ex) {
                logger.fatal(myRank + " - Error: " + ex);
            }
        }

    }

    @Override
    protected EDGEStatImpl EM(List<BDD> BDDs, BigDecimal[] probs) {

        logger.info(myRank + " - Start EM Algorithm\n\t- n. of probabilistic axioms:\t" + rulesName.size()
                + "\n\t- n. of examples:\t\t" + (getExamples().size()));

        int lenNodes = BDDs.size(), nRules = rulesName.size();
        BigDecimal CLL0 = new BigDecimal("-22000000000");  // -inf
        CLL0 = CLL0.setScale(getAccuracy(),  RoundingMode.HALF_UP);
        BigDecimal CLL1 = new BigDecimal("-170000000");    // +inf
        CLL1 = CLL1.setScale(getAccuracy(),  RoundingMode.HALF_UP);
        BigDecimal ratio, diff;
        int cycle = 0;

        List<BigDecimal[]> eta = new ArrayList<>();
        for (int i = 0; i < nRules; i++) {
            BigDecimal[] eta_int_temp = new BigDecimal[2];
            eta_int_temp[0] = new BigDecimal(BigInteger.ZERO);
            eta_int_temp[1] = new BigDecimal(BigInteger.ZERO);
            eta.add(eta_int_temp);

        }

        if (getExample_weights() == null || getExample_weights().length == 0) {
            BigDecimal[] example_weights_local = new BigDecimal[lenNodes];
            for (int i = 0; i < lenNodes; i++) {
                example_weights_local[i] = new BigDecimal(BigInteger.ONE);
            }
            setExample_weights(example_weights_local);
        }
        //System.arraycopy(example_prob_calc, 0, example_prob, 0, lenNodes);

        diff = CLL1.subtract(CLL0);
        ratio = diff.divide(CLL0.abs(), getAccuracy(), RoundingMode.HALF_UP);
        // Expectation-Maximization cycle
        while (checkEMConditions(diff, ratio, cycle)) {
            List<BigDecimal[]> eta_local = new ArrayList<>(eta.size());
            cycle++;
            logger.debug(myRank + " - EM cycle: " + cycle);
            CLL0 = CLL1;
            CLL1 = expectation(BDDs, getExample_weights(), eta, lenNodes, eta_local);
            maximization(eta_local);
            diff = CLL1.subtract(CLL0);
            ratio = diff.divide(CLL0.abs(), getAccuracy(), RoundingMode.HALF_UP);
            logger.debug("Log-likelihood: " + CLL1 + " cycle: " + cycle);
            //System.out.println("Log-Likelyhood ciclo " + cycle + " : " + CLL1);
        }

        logger.info("EM completed.");
        logger.info("\n  Final Log-Likelihood: " + CLL1);

        return new EDGEStatImpl(cycle, CLL1, rulesName, arrayProb);
    }

    protected BigDecimal expectation(List<BDD> nodes_ex, BigDecimal[] example_weights,
            List<BigDecimal[]> eta, int lenNodes, List<BigDecimal[]> eta_local) {
        int nRules = rulesName.size();
        if (MASTER) {
            BigDecimal LL; // = new BigDecimal(0).setScale(getAccuracy(),  RoundingMode.HALF_UP);

            logger.debug(myRank + " - Computing Expectation");
            LL = super.expectation(nodes_ex, example_weights, eta, lenNodes);

            logger.debug(myRank + " - Receiving Expectation results...");
            int numSlaves;
            try {
                numSlaves = MPIUtilities.getSlaves(MPI.COMM_WORLD);
            } catch (MPIException ex) {
                String msg = "Cannot get the current number of slaves";
                logger.error(msg);
                throw new RuntimeException(ex);
            }
            for (BigDecimal[] eta_i : eta) {
                BigDecimal[] eta_i_local = {eta_i[0], eta_i[1]};
                eta_local.add(eta_i_local);
            }
            for (int j = 1; j <= numSlaves; j++) {
                int pos = 0;
                // eta + LLj
                BigDecimal LLj;
                List<BigDecimal[]> etaj;
                try {
                    int recvByteCount = MPI.COMM_WORLD.probe(j, 0).getCount(MPI.PACKED);
                    byte[] recvBuffer = new byte[recvByteCount];
                    MPI.COMM_WORLD.recv(recvBuffer, recvBuffer.length, MPI.PACKED, j, 0);
                    
                    int[] etaArrayLen = new int[1];
                    int posUnpack = MPI.COMM_WORLD.unpack(recvBuffer, 0, etaArrayLen, 1, MPI.INT);
                    byte[] BDBuffer = new byte[etaArrayLen[0]];
                    posUnpack = MPI.COMM_WORLD.unpack(recvBuffer, posUnpack, BDBuffer, etaArrayLen[0], MPI.BYTE);
                    etaj = (List<BigDecimal[]>)MPIUtilities.byteArrayToObject(BDBuffer);
                    
                    BDBuffer = new byte[recvByteCount - posUnpack];
                    MPI.COMM_WORLD.unpack(recvBuffer, posUnpack, BDBuffer, BDBuffer.length, MPI.BYTE);
                    LLj = (BigDecimal)MPIUtilities.byteArrayToObject(BDBuffer);
                    
                    //MPI.COMM_WORLD.recv(longBuffer, (2 * nRules), MPI.LONG, j, 0);
                    //LLj = (BigDecimal)(MPIUtilities.recvObject(j, 0).getObj());
                } catch (MPIException ex) {
                    logger.error(myRank + " - Error: " + ex.getMessage());
                    throw new RuntimeException(ex);
                }
                for (int i = 0; i < nRules * 2; i = i + 2, pos++) {
                    eta_local.get(pos)[0] = eta_local.get(pos)[0].add(etaj.get(pos)[0]);
                    eta_local.get(pos)[1] = eta_local.get(pos)[1].add(etaj.get(pos)[1]);
                }
                logger.debug(myRank + " - LL from " + j + " : " + LLj);
                LL = LL.add(LLj);
                logger.debug(myRank + " - LL after " + j + " : " + LL);

            }
            return LL;
        } else { // slave
            logger.debug(myRank + " - Starting Expectation step...");
            BigDecimal LL = super.expectation(nodes_ex, example_weights, eta, lenNodes);
            logger.debug(myRank + " - Sending expectation results...");
            
            
            try {
                logger.debug(myRank + " - Sending eta (" + (2 * nRules) + " values) + LL: " + LL);
                byte[] LLBuffer = MPIUtilities.objectToByteArray(LL);
                byte[] etaBuffer = MPIUtilities.objectToByteArray(eta);
                byte[] sendBuffer = new byte[(Integer.SIZE / 8) + etaBuffer.length + LLBuffer.length];
                int[] sizeEtaBuffer = {etaBuffer.length};
                int pos = MPI.COMM_WORLD.pack(sizeEtaBuffer, 1, MPI.INT, sendBuffer, 0);
                pos = MPI.COMM_WORLD.pack(etaBuffer, etaBuffer.length, MPI.BYTE, sendBuffer, pos);
                MPI.COMM_WORLD.pack(LLBuffer, LLBuffer.length, MPI.BYTE, sendBuffer, pos);
                MPI.COMM_WORLD.send(sendBuffer, sendBuffer.length, MPI.PACKED, 0, 0);
            } catch (MPIException ex) {
                logger.error(myRank + " - Error: " + ex);
                throw new RuntimeException(ex);
            }
            return LL;
        }
    }

    @Override
    protected void maximization(List<BigDecimal[]> eta) {
        logger.debug(myRank + " - maximization...");
        if (MASTER) {
            super.maximization(eta);
            // send arrayProb to the slave. arrayProb contains the probabilities 
            // of the probabilistic axioms
            //logger.debug(myRank + " - sending to slaves new probabilistic "
            //        + "values for the axioms: [" + arrayProb + "]");
            try {
                MPIUtilities.sendObject(arrayProb, ALL, 0, MPI.COMM_WORLD);
            } catch (MPIException mpiEx) {
                logger.error(myRank + " - Error: " + mpiEx);
                throw new RuntimeException(mpiEx);
            }
        } else {
            // receive array of probabilities of the probabilistic axioms
            try {
                logger.debug(myRank + " - Receiving new values for probabilistic axioms");
                
                arrayProb = (List<BigDecimal>)(MPIUtilities.recvObject(0, 0, MPI.COMM_WORLD).getObj());
                logger.debug(myRank + " - new values: [" + arrayProb + "]");
            } catch (MPIException mpiEx) {
                logger.error(myRank + " - Error: " + mpiEx);
                throw new RuntimeException(mpiEx);
            }
            // set probs_ex
            for (int i = 0; i < probs_ex.size(); i++) {
                for (int j = 0; j < probs_ex.get(i).size(); j++) {
                    BigDecimal prob = arrayProb.get(vars_ex.get(i).get(j));
                    probs_ex.get(i).set(j, prob);
                }
            }
        }
    }

    @Override
    protected boolean checkEMConditions(BigDecimal diffLL, BigDecimal ratioLL, long iter) {
        if (MASTER) {
            if ((diffLL.compareTo(this.getDiffLL()) > 0) && (ratioLL.compareTo(this.getRatioLL()) > 0)
                && (iter < this.getMaxIterations())) {
                try {
                    logger.debug(myRank + " - Cycle " + iter + " : Continue...");
                    MPIUtilities.sendSignal(ALL_BCAST, CONTINUE, MPI.COMM_WORLD);
                    
                } catch (MPIException ex) {
                    logger.fatal(ex);
                    System.exit(-2);
                }
                return true;
            } else {
                try {
                    logger.debug(myRank + " - Cycle " + iter + " : Stop...");
                    MPIUtilities.sendSignal(ALL_BCAST, STOP, MPI.COMM_WORLD);
                    
                } catch (MPIException ex) {
                    logger.fatal(ex);
                    System.exit(-2);
                }
                return false;
            }
        } else { // slave
            int[] signal = new int[1];
            try {
                logger.debug(myRank + " - Waiting for signal...");
                // MAYBE NOT BROADCAST
                //MPI.COMM_WORLD.recv(signal, 1, MPI.INT, 0, 0);
                MPI.COMM_WORLD.bcast(signal, 1, MPI.INT, 0);
                logger.debug(myRank + " - Received: " + signal[0]);
            } catch (MPIException ex) {
                logger.error(myRank + " - Error: " + ex);
                throw new RuntimeException(ex);
            }
            boolean continueEM = (signal[0] == CONTINUE);
            logger.debug(myRank + " - run flag: " + continueEM);
            return continueEM;
        }
    }

    @Override
    protected void printLearningResult(Timers timers) { //, BDDFactory.CacheStats fCacheStats, BDDFactory.GCStats fGCStats, BDDFactory.ReorderStats fReorderStats) {
        if (MASTER) {
            super.printLearningResult(timers); //, fCacheStats, fGCStats, fReorderStats);
        }
    }

    private class ExamplesSender implements Runnable {

        private final int slave;

        public ExamplesSender(int slave) {
            super();
            this.slave = slave;
        }

        @Override
        public void run() {
            try {
                //int posExamples = positiveExamples.size();
                int pos = 0;
                int numPosExamples = 0;
                for (int i = 0; i < slave; i++) {
                    pos += examplesDistribution.get(i).size();
                }

                for (Map.Entry<OWLAxiom, Type> entry : examplesDistribution.get(slave)) {
                    if (entry.getValue() == Type.POSITIVE) {
                        numPosExamples++;
                    }
                }

                int numSlaveExamples = examplesDistribution.get(slave).size();
                logger.debug(myRank + " - send to " + slave + " " + numSlaveExamples
                        + " examples (positive " + numPosExamples + " negative " + (numSlaveExamples - numPosExamples) + "):");
                int[] dimt = {numSlaveExamples, numPosExamples};
                MPI.COMM_WORLD.send(dimt, 2, MPI.INT, slave, 0);

                for (Map.Entry<OWLAxiom, Type> entry : examplesDistribution.get(slave)) {
                    logger.debug(myRank + " - example (array pos.: " + pos + " - tag: " + (pos + 1) + " )");
                    MPIUtilities.sendObject(entry.getKey(), slave, pos + 1, MPI.COMM_WORLD);
                    pos++;

//                    int[] signal = new int[1];
//                    try {
//                        MPI.COMM_WORLD.recv(signal, 1, MPI.INT, j, 0);
//                    } catch (MPIException ex) {
//                        logger.error("");
//                    }
//                    boolean runFlag = (signal[0] == MPIUtilities.CONTINUE);
//                    logger.debug(myRank + " - runFlag = " + runFlag);
                }

            } catch (MPIException ex) {
                logger.fatal(myRank + " - Error: " + ex);
                throw new RuntimeException(ex);
            } catch (Exception ex) {
                logger.error(myRank + " - Error: " + ex);
                throw new RuntimeException(ex);
            }
        }

    }

}

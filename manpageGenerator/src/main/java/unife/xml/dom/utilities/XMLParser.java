/**
 * This file is part of LEAP.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * LEAP is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * LEAP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package unife.xml.dom.utilities;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Giuseppe Cota <giuseta@gmail.com>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class XMLParser {

    /**
     *
     * @param parent
     * @param tagName
     * @return a list containing the elements with the corresponding tag name.
     * If no element has the given tag name, returns an empty list.
     */
    public List<Element> getChildElements(Element parent, String tagName) {
        NodeList children = parent.getChildNodes();
        List<Element> childElementsList = new ArrayList<>();
        if (children != null) {
            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);
                if (child instanceof Element && ((Element) child).getTagName().equals(tagName)) {
                    childElementsList.add((Element) child);
                }
            }
        }
        return childElementsList;
    }

    /**
     *
     * @param parent parent element
     * @param tagName tag name of the first child
     * @return the first child element with the corresponding tag name. If there
     * is no element with the given tagName
     */
    public Element getFirstChildElement(Element parent, String tagName) {
        NodeList children = parent.getChildNodes();
        Element firstChildElement = null;
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element && ((Element) child).getTagName().equals(tagName)) {
                firstChildElement = (Element) child;
                return firstChildElement;
            }
        }
        return firstChildElement;
    }

    public List<Node> getChildNodes(Node parent) {
        NodeList children = parent.getChildNodes();
        List<Node> childNodesList = new ArrayList<>();
        for (int i = 0; i < children.getLength(); i++) {
            childNodesList.add(children.item(i));
        }
        return childNodesList;
    }

}

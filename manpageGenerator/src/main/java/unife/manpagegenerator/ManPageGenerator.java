/**
 * This file is part of LEAP.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * LEAP is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 *
 * LEAP is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package unife.manpagegenerator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.cli.ParseException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import unife.manpagegenerator.exception.CorrespondingOptionNotFound;
import unife.xml.dom.utilities.XMLParser;

/**
 * This class is used in order to generate manual pages. The information used to
 * generate the man page is read from an XML file.
 *
 */
@Mojo(name = "mangen", defaultPhase = LifecyclePhase.SITE)
public class ManPageGenerator extends AbstractMojo {

    @Parameter(property = "manpage.sourceXML", defaultValue = "")
    public File sourceXML;

    private XMLParser parser = new XMLParser();

    public void execute() throws MojoExecutionException {
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = db.parse(sourceXML);
            Element root = doc.getDocumentElement();
            String section = root.getAttribute("section");
            if (section != null) {
                section = section.trim();
            } else {
                section = "1";
            }
            String command = root.getTagName();
            File manFile = new File("man/man" + section + "/" + command + "." + section);
            manFile.getParentFile().mkdirs();
            BufferedWriter writer = new BufferedWriter(new FileWriter(manFile));
            // write comments
            writer.write(".\\\" Manpage for edge.");
            writer.newLine();
            writer.write(".\\\" Contact ctogpp@unife.it to correct errors or typos.");
            writer.newLine();
            // write header
            writer.write(".TH " + command + " " + section + " \"" + getCurrentDate() + "\"" + " \"" + command + " man page\"");
            writer.newLine();
            // create the NAME section
            appendNameSection(root, writer);
            // create SYNOPSYS section           
            appendSynopsysSection(root, writer);
            // create DESCRIPTION section
            appendDecriptionSection(root, writer);
            // create OPTIONS section
            appendOptionsSection(root, writer);
            // create BUGS section
            appendBugsSection(root, writer);
            // create AUTHORS section
            appendAuthorsSection(root, writer);

            writer.close();
        } catch (IOException | ParserConfigurationException | SAXException |
                CorrespondingOptionNotFound | ParseException ex) {
            System.err.println("Error: " + ex.getMessage());
            throw new RuntimeException(ex);
        }
    }

    private String getCurrentDate() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd MMM yyyy");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    private void appendNameSection(Element root, BufferedWriter writer) throws IOException {
        Element name = parser.getFirstChildElement(root, "name");
        if (name != null) {
            writer.write(".SH NAME");
            writer.newLine();
            writer.write(root.getTagName() + " \\- " + name.getTextContent().trim());
            writer.newLine();
        }
    }

    private void appendSynopsysSection(Element root, BufferedWriter writer) throws ParseException, IOException, CorrespondingOptionNotFound {
        Element synopsys = parser.getFirstChildElement(root, "synopsys");
        // get the options
        Element optionsEl = parser.getFirstChildElement(root, "options");
        List<Element> options = null;
        if (optionsEl != null) {
            options = parser.getChildElements(optionsEl, "option");
        }
        if (synopsys != null) {
            writer.write(".SH SYNOPSYS");
            writer.newLine();
            Element usages = parser.getFirstChildElement(synopsys, "usages");
            if (usages != null) {
                for (Element usage : parser.getChildElements(usages, "usage")) {
                    writer.write(".B " + root.getTagName());
                    writer.newLine();
                    if (parser.getFirstChildElement(usage, "optional") != null) {
                        Element optional = parser.getFirstChildElement(usage, "optional");
                        writer.write(".BI [ " + "\"" + optional.getTextContent() + "\" ]");
                        writer.newLine();
                    }
                    Element required = parser.getFirstChildElement(usage, "required");
                    if (required != null) {
                        Element requiredOptionsEl = parser.getFirstChildElement(required, "options");
                        List<Element> requiredOptions = parser.getChildElements(requiredOptionsEl, "option");
                        for (Element requiredOption : requiredOptions) {
                            Element correspondingOption = null;
                            if (options == null) {
                                String msg = "The corresponding required option"
                                        + " defined on line " + requiredOption.getUserData("lineNumber")
                                        + " does not exist";
                                throw new CorrespondingOptionNotFound(msg);
                            }
                            for (Element option : options) {
                                if (option.getAttribute("name").equals(requiredOption.getAttribute("name"))) {
                                    correspondingOption = option;
                                    break;
                                }
                            }
                            if (correspondingOption == null) {
                                String msg = "The corresponding required option"
                                        + " defined on line " + requiredOption.getUserData("lineNumber")
                                        + " does not exist";
                                throw new CorrespondingOptionNotFound(msg);
                            }
                            writer.write(".RI " + "\"-" + correspondingOption.getAttribute("alias") + " \" "
                                    + "\"" + correspondingOption.getAttribute("arg-name") + "\"");
                            writer.newLine();
                        }
                        Element argsEl = parser.getFirstChildElement(required, "args");
                        if ( argsEl != null) {
                            for (Element arg : parser.getChildElements(argsEl, "arg")) {
                                writer.write(".I " + arg.getTextContent().trim());
                                writer.newLine();
                            }
                        }
                    }
                    writer.newLine();
                }

            } else {
                writer.write(".B " + root.getTagName());
                writer.newLine();
            }
        }
    }

    private void appendDecriptionSection(Element root, BufferedWriter writer) throws IOException {
        Element description = parser.getFirstChildElement(root, "description");
        if (description != null) {
            writer.write(".SH DESCRIPTION");
            writer.newLine();
            List<Node> childNodes = parser.getChildNodes(description);
            for (Node child : childNodes) {
                if (child instanceof Text) {
                    writer.write(child.getTextContent().trim());
                    writer.newLine();
                } else if (child instanceof Element) {
                    if (((Element) child).getTagName().equals(root.getTagName())) {
                        writer.write(".B " + root.getTagName());
                        writer.newLine();
                    }
                }
            }
        }
    }

    private void appendOptionsSection(Element root, BufferedWriter writer) throws IOException {
        Element optionsEl = parser.getFirstChildElement(root, "options");
        if (optionsEl != null) {
            writer.write(".SH OPTIONS");
            writer.newLine();
            for (Element option : parser.getChildElements(optionsEl, "option")) {
                writer.write(".TP");
                writer.newLine();
                String longOpt = option.getAttribute("name");
                String opt = option.getAttribute("alias");
                String argName = option.getAttribute("arg-name");
                writer.write(".BI \"");
                if (!opt.isEmpty()) {
                    writer.write("-" + opt + ", ");
                }
                writer.write("--" + longOpt + " \" " + "\"" + argName + "\"");
                writer.newLine();

                Element description = parser.getFirstChildElement(option, "description");
                String descriptionStr = "";
                if (description != null) {
                    for (Node child : parser.getChildNodes(description)) {
                        if (child instanceof Text) {
                            writer.write(child.getTextContent().trim());
                            writer.newLine();
                        } else if (child instanceof Element) {
                            if (((Element) child).getTagName().equals("arg-name")) {
                                writer.write(".I " + argName);
                                writer.newLine();
                            }
                        }
                    }

                    List<Element> args = parser.getChildElements(option, "arg");
                    if (args.size() > 0) {
                        descriptionStr += ". Possible arguments: ";
                    }
                    for (Element arg : args) {
                        descriptionStr += arg.getTextContent().trim() + " ";
                    }

                    writer.write(descriptionStr);
                    writer.newLine();
                }
            }
        }
    }

    private void appendBugsSection(Element root, BufferedWriter writer) throws IOException {
        Element bugs = parser.getFirstChildElement(root, "bugs");
        if (bugs != null) {
            writer.write(".SH BUGS");
            writer.newLine();
            writer.write("Report");
            writer.newLine();
            writer.write(".B " + root.getTagName());
            writer.newLine();
            writer.write("bugs ");
            List<Element> emails = parser.getChildElements(bugs, "email");
            int i = 0;
            for (Element email : emails) {
                if (i == 0) {
                    writer.write("to ");
                } else {
                    writer.write("or ");
                }
                writer.newLine();
                writer.write(".I " + email.getTextContent());
                writer.newLine();
            }
            writer.newLine();
        }
    }

    private void appendAuthorsSection(Element root, BufferedWriter writer) throws IOException {
        Element authorsEl = parser.getFirstChildElement(root, "authors");
        if (authorsEl != null) {
            writer.write(".SH AUTHORS");
            writer.newLine();
            List<Element> authors = parser.getChildElements(authorsEl, "author");
            writer.write("Written by ");
            int i = 0;
            for (Element author : authors) {
                if (i > 0) {
                    writer.write(" and ");
                }
                writer.write(author.getTextContent());
                i++;
            }
            writer.newLine();
        }
    }

}

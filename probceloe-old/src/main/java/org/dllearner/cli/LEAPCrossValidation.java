/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.dllearner.cli;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import org.apache.log4j.Logger;
//import org.dllearner.core.AbstractCELA;
import org.dllearner.core.AbstractLearningProblem;
import org.dllearner.core.AbstractReasonerComponent;
import org.dllearner.core.ComponentInitException;
import org.dllearner.core.owl.Description;
import org.dllearner.core.owl.Individual;
import org.dllearner.learningproblems.ClassLearningProblem;
//import org.dllearner.learningproblems.Heuristics;
import org.dllearner.learningproblems.PosNegLP;
import org.dllearner.learningproblems.PosOnlyLP;
import org.dllearner.probabilistic.algorithms.celoe.ProbCELOE;
import org.dllearner.probabilistic.edge.EDGE;
import org.dllearner.utilities.Files;
import org.dllearner.utilities.Helper;
import org.dllearner.utilities.ReflectionHelper;
import org.dllearner.utilities.datastructures.Datastructures;
import org.dllearner.utilities.statistics.Stat;
import org.apache.commons.io.FilenameUtils;
import org.dllearner.core.owl.Thing;

/**
 * Performs a pseudo cross validation for the given problem using the LEAP
 * system. It is not a real k-fold cross validation, because this class executes
 * only a k-fold training. It produces k output file which must be submitted to
 * testing
 *
 * @author Jens Lehmann
 * @author Giuseppe Cota <giuseta@gmail.com>
 */
public class LEAPCrossValidation {

    private static final Logger logger = Logger.getLogger(LEAPCrossValidation.class);

    // statistical values
    protected Stat runtime = new Stat();
    protected Stat accuracy = new Stat();
    protected Stat length = new Stat();
    protected Stat accuracyTraining = new Stat();
    protected Stat fMeasure = new Stat();
    protected Stat fMeasureTraining = new Stat();
    protected static boolean writeToFile = false;
    protected static File outputFile;

    protected Stat trainingCompletenessStat = new Stat();
    protected Stat trainingCorrectnessStat = new Stat();

    protected Stat testingCompletenessStat = new Stat();
    protected Stat testingCorrectnessStat = new Stat();

    public LEAPCrossValidation(ProbCELOE pceloe, EDGE edge, AbstractLearningProblem lp, AbstractReasonerComponent rs, int folds, boolean leaveOneOut) {

        DecimalFormat df = new DecimalFormat();

        // the training sets used later on
        List<Set<Individual>> trainingSetsPos = new LinkedList<Set<Individual>>();
        List<Set<Individual>> trainingSetsNeg = new LinkedList<Set<Individual>>();

        // get individuals and shuffle them too
        Set<Individual> posExamples = new HashSet();
        Set<Individual> negExamples = new HashSet();
        logger.debug("Setting cross validation");
        if (lp instanceof PosNegLP) {
            posExamples = ((PosNegLP) lp).getPositiveExamples();
            negExamples = ((PosNegLP) lp).getNegativeExamples();
        } else if (lp instanceof PosOnlyLP) {
            posExamples = ((PosNegLP) lp).getPositiveExamples();
            //negIndividuals = Helper.difference(lp.getReasoner().getIndividuals(), posExamples);
            negExamples = new HashSet<Individual>();
        } else if (lp instanceof ClassLearningProblem) {
            try {
                posExamples = new HashSet((List<Individual>) ReflectionHelper.getPrivateField(lp, "classInstances"));
                negExamples = new HashSet((List<Individual>) ReflectionHelper.getPrivateField(lp, "superClassInstances"));
                // if the number of negative examples is lower than the number of folds 
                // get as negative examples all the individuals that are not instances of ClassToDescribe
                if (negExamples.size() < folds) {
                    logger.info("The number of folds is higher than the number of "
                    + "negative examples. Selecting the instances of Thing which "
                            + "are non instances of ClasstoDescribe as negative Examples");
                    AbstractReasonerComponent reasoner = lp.getReasoner();
                    // get as negative examples all the individuals which belong to the class Thing
                    // but not to the ClassToDescribe
                    negExamples = reasoner.getIndividuals(Thing.instance);
                    negExamples.removeAll(posExamples);
                }
            } catch (Exception e) {
                logger.error("Cannot get positive and negative individuals for the cross validation");
                logger.error(e);
                System.exit(-2);
            }
        } else {
            throw new IllegalArgumentException("Only ClassLearningProblem and PosNeg and PosOnly learning problems are supported");
        }
        List<Individual> posExamplesList = new LinkedList<Individual>(posExamples);
        List<Individual> negExamplesList = new LinkedList<Individual>(negExamples);
        Collections.shuffle(posExamplesList, new Random(1));
        Collections.shuffle(negExamplesList, new Random(2));

        // sanity check whether nr. of folds makes sense for this benchmark
        if (!leaveOneOut && (posExamples.size() < folds || negExamples.size() < folds)) {
            logger.error("The number of folds is higher than the number of "
                    + "positive/negative examples. This can result in empty test sets. Exiting.");
            System.exit(0);
        }

        if (leaveOneOut) {
            // note that leave-one-out is not identical to k-fold with
            // k = nr. of examples in the current implementation, because
            // with n folds and n examples there is no guarantee that a fold
            // is never empty (this is an implementation issue)
            int nrOfExamples = posExamples.size() + negExamples.size();
            for (int i = 0; i < nrOfExamples; i++) {
                // ...
            }
            logger.error("Leave-one-out not supported yet.");
            System.exit(1);
        } else {
            // calculating where to split the sets, ; note that we split
            // positive and negative examples separately such that the 
            // distribution of positive and negative examples remains similar
            // (note that there are better but more complex ways to implement this,
            // which guarantee that the sum of the elements of a fold for pos
            // and neg differs by at most 1 - it can differ by 2 in our implementation,
            // e.g. with 3 folds, 4 pos. examples, 4 neg. examples)
            int[] splitsPos = calculateSplits(posExamples.size(), folds);
            int[] splitsNeg = calculateSplits(negExamples.size(), folds);

//				System.out.println(splitsPos[0]);
//				System.out.println(splitsNeg[0]);
            // calculating training and test sets
            for (int i = 0; i < folds; i++) {
                Set<Individual> testPos = getTestingSet(posExamplesList, splitsPos, i);
                Set<Individual> testNeg = getTestingSet(negExamplesList, splitsNeg, i);
                //testSetsPos.add(i, testPos);
                //testSetsNeg.add(i, testNeg);
                trainingSetsPos.add(i, getTrainingSet(posExamples, testPos));
                trainingSetsNeg.add(i, getTrainingSet(negExamples, testNeg));
            }

        }

        String completeLearnedOntology = pceloe.getCompleteLearnedOntology();
        String cloBase = FilenameUtils.removeExtension(completeLearnedOntology);
        String cloExt = FilenameUtils.getExtension(completeLearnedOntology);

        String positiveFile = edge.getPositiveFile();
        String pfBase = FilenameUtils.removeExtension(positiveFile);
        String pfExt = FilenameUtils.getExtension(positiveFile);
        String negativeFile = edge.getNegativeFile();
        String nfBase = FilenameUtils.removeExtension(negativeFile);
        String nfExt = FilenameUtils.getExtension(negativeFile);

        // run the algorithm
        for (int currFold = 0; currFold < folds; currFold++) {

            // setting positive and negative individuals
            Set<String> pos = Datastructures.individualSetToStringSet(trainingSetsPos.get(currFold));
            Set<String> neg = Datastructures.individualSetToStringSet(trainingSetsNeg.get(currFold));
            if (PosNegLP.class.isAssignableFrom(lp.getClass())) {
                ((PosNegLP) lp).setPositiveExamples(trainingSetsPos.get(currFold));
                ((PosNegLP) lp).setNegativeExamples(trainingSetsNeg.get(currFold));
                try {
                    lp.init();
                } catch (ComponentInitException e) {
                    logger.error(e);
                    logger.error(e.getLocalizedMessage());
                    System.exit(-2);
                }
            } else if (lp instanceof PosOnlyLP) {
                // il cross training viene fatto solo per gli esempi/individui positivi
                ((PosOnlyLP) lp).setPositiveExamples(new TreeSet<Individual>(trainingSetsPos.get(currFold)));
                try {
                    lp.init();
                } catch (ComponentInitException e) {
                    logger.error(e);
                    logger.error(e.getLocalizedMessage());
                    System.exit(-2);
                }
                // set negative f
            } else if (lp instanceof ClassLearningProblem) {
                try {
                    // Initialize the ClassLearningProblem object first and then 
                    // modify his  private fields
                    //lp.init();
                    ReflectionHelper.setPrivateField(lp, "classInstances", new LinkedList(trainingSetsPos.get(currFold)));
                    ReflectionHelper.setPrivateField(lp, "superClassInstances", new LinkedList(trainingSetsNeg.get(currFold)));
                    ReflectionHelper.setPrivateField(lp, "negatedClassInstances", trainingSetsNeg.get(currFold));
                } catch (Exception e) {
                    logger.error("Cannot set positive and negative individuals for the cross validation");
                    logger.error(e);
                    System.exit(-2);
                }
            }

            try {
                rs.init();
                pceloe.setCompleteLearnedOntology(cloBase + (currFold + 1) + "." + cloExt);
                pceloe.init();
                edge.setPositiveFile(pfBase + (currFold + 1) + "." + pfExt);
                edge.setNegativeFile(nfBase + (currFold + 1) + "." + nfExt);
                edge.init();
            } catch (ComponentInitException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//            long algorithmStartTime = System.nanoTime();      
            pceloe.setTerminateEDGEServerAfterRun(false);
            pceloe.start();
//            long algorithmDuration = System.nanoTime() - algorithmStartTime;
//            runtime.addNumber(algorithmDuration / (double) 1000000000);
//
//            Description concept = pceloe.getCurrentlyBestDescription();
//
//            Set<Individual> tmp = rs.hasType(concept, testSetsPos.get(currFold));
//            Set<Individual> tmp2 = Helper.difference(testSetsPos.get(currFold), tmp);
//            Set<Individual> tmp3 = rs.hasType(concept, testSetsNeg.get(currFold));
//
//            outputWriter("test set errors pos: " + tmp2);
//            outputWriter("test set errors neg: " + tmp3);
//
//            // calculate training accuracies 
//            int trainingCorrectPosClassified = getCorrectPosClassified(rs, concept, trainingSetsPos.get(currFold));
//            int trainingCorrectNegClassified = getCorrectNegClassified(rs, concept, trainingSetsNeg.get(currFold));
//            int trainingCorrectExamples = trainingCorrectPosClassified + trainingCorrectNegClassified;
//            double trainingAccuracy = 100 * ((double) trainingCorrectExamples / (trainingSetsPos.get(currFold).size()
//                    + trainingSetsNeg.get(currFold).size()));
//            accuracyTraining.addNumber(trainingAccuracy);
//            // calculate test accuracies
//            int correctPosClassified = getCorrectPosClassified(rs, concept, testSetsPos.get(currFold));
//            int correctNegClassified = getCorrectNegClassified(rs, concept, testSetsNeg.get(currFold));
//            int correctExamples = correctPosClassified + correctNegClassified;
//            double currAccuracy = 100 * ((double) correctExamples / (testSetsPos.get(currFold).size()
//                    + testSetsNeg.get(currFold).size()));
//            accuracy.addNumber(currAccuracy);
//            // calculate training F-Score
//            int negAsPosTraining = rs.hasType(concept, trainingSetsNeg.get(currFold)).size();
//            double precisionTraining = trainingCorrectPosClassified + negAsPosTraining == 0 ? 0 : trainingCorrectPosClassified / (double) (trainingCorrectPosClassified + negAsPosTraining);
//            double recallTraining = trainingCorrectPosClassified / (double) trainingSetsPos.get(currFold).size();
//            fMeasureTraining.addNumber(100 * Heuristics.getFScore(recallTraining, precisionTraining));
//            // calculate test F-Score
//            int negAsPos = rs.hasType(concept, testSetsNeg.get(currFold)).size();
//            double precision = correctPosClassified + negAsPos == 0 ? 0 : correctPosClassified / (double) (correctPosClassified + negAsPos);
//            double recall = correctPosClassified / (double) testSetsPos.get(currFold).size();
////			System.out.println(precision);System.out.println(recall);
//            fMeasure.addNumber(100 * Heuristics.getFScore(recall, precision));
//
//            length.addNumber(concept.getLength());
//
//            outputWriter("fold " + currFold + ":");
//            outputWriter("  training: " + pos.size() + " positive and " + neg.size() + " negative examples");
//            outputWriter("  testing: " + correctPosClassified + "/" + testSetsPos.get(currFold).size() + " correct positives, "
//                    + correctNegClassified + "/" + testSetsNeg.get(currFold).size() + " correct negatives");
//            outputWriter("  concept: " + concept);
//            outputWriter("  accuracy: " + df.format(currAccuracy) + "% (" + df.format(trainingAccuracy) + "% on training set)");
//            outputWriter("  length: " + df.format(concept.getLength()));
//            outputWriter("  runtime: " + df.format(algorithmDuration / (double) 1000000000) + "s");

        }

        // terminate server, comment this line if you don't want to kill the EDGE server
        edge.stop();

//        outputWriter("");
//        outputWriter("Finished " + folds + "-folds cross-validation.");
//        outputWriter("runtime: " + statOutput(df, runtime, "s"));
//        outputWriter("length: " + statOutput(df, length, ""));
//        outputWriter("F-Measure on training set: " + statOutput(df, fMeasureTraining, "%"));
//        outputWriter("F-Measure: " + statOutput(df, fMeasure, "%"));
//        outputWriter("predictive accuracy on training set: " + statOutput(df, accuracyTraining, "%"));
//        outputWriter("predictive accuracy: " + statOutput(df, accuracy, "%"));
    }

    protected int getCorrectPosClassified(AbstractReasonerComponent rs, Description concept, Set<Individual> testSetPos) {
        return rs.hasType(concept, testSetPos).size();
    }

    protected int getCorrectNegClassified(AbstractReasonerComponent rs, Description concept, Set<Individual> testSetNeg) {
        return testSetNeg.size() - rs.hasType(concept, testSetNeg).size();
    }

    public static Set<Individual> getTestingSet(List<Individual> examples, int[] splits, int fold) {
        int fromIndex;
        // we either start from 0 or after the last fold ended
        if (fold == 0) {
            fromIndex = 0;
        } else {
            fromIndex = splits[fold - 1];
        }
        // the split corresponds to the ends of the folds
        int toIndex = splits[fold];

//		System.out.println("from " + fromIndex + " to " + toIndex);
        Set<Individual> testingSet = new HashSet<Individual>();
        // +1 because 2nd element is exclusive in subList method
        testingSet.addAll(examples.subList(fromIndex, toIndex));
        return testingSet;
    }

    public static Set<Individual> getTrainingSet(Set<Individual> examples, Set<Individual> testingSet) {
        return Helper.difference(examples, testingSet);
    }

    // takes nr. of examples and the nr. of folds for this examples;
    // returns an array which says where each fold ends, i.e.
    // splits[i] is the index of the last element of fold i in the examples
    public static int[] calculateSplits(int nrOfExamples, int folds) {
        int[] splits = new int[folds];
        for (int i = 1; i <= folds; i++) {
            // we always round up to the next integer
            splits[i - 1] = (int) Math.ceil(i * nrOfExamples / (double) folds);
        }
        return splits;
    }

    public static String statOutput(DecimalFormat df, Stat stat, String unit) {
        String str = "av. " + df.format(stat.getMean()) + unit;
        str += " (deviation " + df.format(stat.getStandardDeviation()) + unit + "; ";
        str += "min " + df.format(stat.getMin()) + unit + "; ";
        str += "max " + df.format(stat.getMax()) + unit + ")";
        return str;
    }

    public Stat getAccuracy() {
        return accuracy;
    }

    public Stat getLength() {
        return length;
    }

    public Stat getRuntime() {
        return runtime;
    }

    protected void outputWriter(String output) {
        if (writeToFile) {
            Files.appendToFile(outputFile, output + "\n");
            System.out.println(output);
        } else {
            System.out.println(output);
        }

    }

    public Stat getfMeasure() {
        return fMeasure;
    }

    public Stat getfMeasureTraining() {
        return fMeasureTraining;
    }
}

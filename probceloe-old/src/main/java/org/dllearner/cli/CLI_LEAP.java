/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package org.dllearner.cli;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Level;
import org.apache.xmlbeans.XmlObject;
import org.dllearner.algorithms.ParCEL.ParCELPosNegLP;
import org.dllearner.configuration.IConfiguration;
import org.dllearner.configuration.spring.ApplicationContextBuilder;
import org.dllearner.configuration.spring.DefaultApplicationContextBuilder;
import org.dllearner.configuration.util.SpringConfigurationXMLBeanConverter;
import org.dllearner.confparser3.ConfParserConfiguration;
import org.dllearner.confparser3.ParseException;
import org.dllearner.core.*;
import org.dllearner.learningproblems.PosNegLP;
import org.dllearner.probabilistic.algorithms.celoe.ProbCELOE;
import org.dllearner.probabilistic.edge.EDGE;
import org.dllearner.utilities.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

/**
 *
 * Command line interface for LEAP, based on DLLearner CLI
 *
 * @author Jens Lehmann
 * @author Giuseppe Cota
 *
 */
public class CLI_LEAP extends CLI {

    private static Logger logger = LoggerFactory.getLogger(CLI_LEAP.class);

    private ApplicationContext context;
    private IConfiguration configuration;
    private File confFile;

    private AbstractCELA algorithm;
    private KnowledgeSource knowledgeSource;

    // some CLI options
    private boolean writeSpringConfiguration = false;
    private boolean performCrossValidation = false;
    private int nrOfFolds = 10;
    private int noOfRuns = 1;

    private String logLevel = "INFO";

    public CLI_LEAP() {

    }

    public CLI_LEAP(File confFile) {
        //this();
        //this.confFile = confFile;
        super(confFile);
    }

    public void run() throws IOException {
        try {
            org.apache.log4j.Logger.getLogger("org.dllearner").setLevel(Level.toLevel(logLevel.toUpperCase()));
        } catch (Exception e) {
            logger.warn("Error setting log level to " + logLevel);
        }

        if (writeSpringConfiguration) {
            SpringConfigurationXMLBeanConverter converter = new SpringConfigurationXMLBeanConverter();
            XmlObject xml;
            if (configuration == null) {
                Resource confFileR = new FileSystemResource(confFile);
                configuration = new ConfParserConfiguration(confFileR);
                xml = converter.convert(configuration);
            } else {
                xml = converter.convert(configuration);
            }
            String springFilename = confFile.getCanonicalPath().replace(".conf", ".xml");
            File springFile = new File(springFilename);
            if (springFile.exists()) {
                logger.warn("Cannot write Spring configuration, because " + springFilename + " already exists.");
            } else {
                Files.createFile(springFile, xml.toString());
            }
        }

        // the following boolean variable indicates if we are using LEAP system or not
        boolean leap = false;
        // There must be 2 learning algorithm in order to be into the LEAP system:
        // ProbCELOE and EDGE
        Map algorithms = context.getBeansOfType(LearningAlgorithm.class);
        if (algorithms.size() == 2) {
            List<LearningAlgorithm> algs = new ArrayList<LearningAlgorithm>();
            for (Entry<String, LearningAlgorithm> entry : context.getBeansOfType(LearningAlgorithm.class).entrySet()) {
                algs.add(entry.getValue());
            }
            // check if the two learning algorithms are ProbCELOE and EDGE
            if ((algs.get(0) instanceof ProbCELOE && algs.get(1) instanceof EDGE)
                    || (algs.get(0) instanceof EDGE && algs.get(1) instanceof ProbCELOE)) {
                leap = true;
            }
        }

        if (performCrossValidation && !leap) {
            AbstractReasonerComponent rs = context.getBean(AbstractReasonerComponent.class);
            AbstractCELA la = context.getBean(AbstractCELA.class);

            //this test is added for PDLL algorithm since it does not use the PosNegLP			
            try {
                ParCELPosNegLP lp = context.getBean(ParCELPosNegLP.class);
                new ParCELCrossValidation(la, lp, rs, nrOfFolds, false, noOfRuns);
            } catch (BeansException be) {
                PosNegLP lp = context.getBean(PosNegLP.class);
                new CrossValidation(la, lp, rs, nrOfFolds, false);
            }

        } else if (performCrossValidation && leap) {
            AbstractReasonerComponent rs = context.getBean(AbstractReasonerComponent.class);
            AbstractLearningProblem lp = context.getBean(AbstractLearningProblem.class);
            ProbCELOE pceloe = context.getBean(ProbCELOE.class);
            EDGE edge = context.getBean(EDGE.class);
            new LEAPCrossValidation(pceloe, edge, lp, rs, nrOfFolds, false);
            // TO DO 

        } else {
//			knowledgeSource = context.getBeansOfType(Knowledge1Source.class).entrySet().iterator().next().getValue();
            for (Entry<String, AbstractCELA> entry : context.getBeansOfType(AbstractCELA.class).entrySet()) {
                algorithm = entry.getValue();
                logger.info("Running algorithm instance \"" + entry.getKey() + "\" (" + algorithm.getClass().getSimpleName() + ")");
                algorithm.start();
            }
        }

    }

    /**
     * @param args
     * @throws ParseException
     * @throws IOException
     * @throws ReasoningMethodUnsupportedException
     */
    public static void main(String[] args) throws ParseException, IOException, ReasoningMethodUnsupportedException {

//		System.out.println("DL-Learner " + Info.build + " [TODO: read pom.version and put it here (make sure that the code for getting the version also works in the release build!)] command line interface");
        System.out.println("DL-Learner command line interface");

        // currently, CLI_LEAP has exactly one parameter - the conf file
        if (args.length == 0) {
            System.out.println("You need to give a conf file as argument.");
            System.exit(0);
        }

        // read file and print and print a message if it does not exist
        File file = new File(args[args.length - 1]);
        if (!file.exists()) {
            System.out.println("File \"" + file + "\" does not exist.");
            System.exit(0);
        }

        Resource confFile = new FileSystemResource(file);

        List<Resource> springConfigResources = new ArrayList<Resource>();

        try {
            //DL-Learner Configuration Object
            IConfiguration configuration = new ConfParserConfiguration(confFile);

            ApplicationContextBuilder builder = new DefaultApplicationContextBuilder();
            ApplicationContext context = builder.buildApplicationContext(configuration, springConfigResources);

            // TODO: later we could check which command line interface is specified in the conf file
            // for now we just use the default one
            CLI cli;
            if (context.containsBean("cli")) {
                //cli = (CLI) context.getBean("cli");
                cli = context.getBean("cli", CLI.class);
            } else {
                cli = new CLI_LEAP();
            }
            cli.setContext(context);
            cli.setConfFile(file);
            cli.run();
        } catch (Exception e) {
            e.printStackTrace();
            String stacktraceFileName = "log/error.log";

//            e.printStackTrace();
            //Find the primary cause of the exception.
            Throwable primaryCause = findPrimaryCause(e);

            // Get the Root Error Message
            logger.error("An Error Has Occurred During Processing.");
            logger.error(primaryCause.getMessage());
            logger.debug("Stack Trace: ", e);
            logger.error("Terminating DL-Learner...and writing stacktrace to: " + stacktraceFileName);
            FileOutputStream fos = new FileOutputStream(stacktraceFileName);
            PrintStream ps = new PrintStream(fos);
            e.printStackTrace(ps);
        }

    }

    /**
     * Find the primary cause of the specified exception.
     *
     * @param e The exception to analyze
     * @return The primary cause of the exception.
     */
    private static Throwable findPrimaryCause(Exception e) {
        // The throwables from the stack of the exception
        Throwable[] throwables = ExceptionUtils.getThrowables(e);

        //Look For a Component Init Exception and use that as the primary cause of failure, if we find it
        int componentInitExceptionIndex = ExceptionUtils.indexOfThrowable(e, ComponentInitException.class);

        Throwable primaryCause;
        if (componentInitExceptionIndex > -1) {
            primaryCause = throwables[componentInitExceptionIndex];
        } else {
            //No Component Init Exception on the Stack Trace, so we'll use the root as the primary cause.
            primaryCause = ExceptionUtils.getRootCause(e);
        }
        return primaryCause;
    }

    @Override
    public boolean isWriteSpringConfiguration() {
        return writeSpringConfiguration;
    }

    @Override
    public void setWriteSpringConfiguration(boolean writeSpringConfiguration) {
        this.writeSpringConfiguration = writeSpringConfiguration;
    }

    @Override
    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public ApplicationContext getContext() {
        return context;
    }

    @Override
    public File getConfFile() {
        return confFile;
    }

    @Override
    public void setConfFile(File confFile) {
        this.confFile = confFile;
    }

    @Override
    public boolean isPerformCrossValidation() {
        return performCrossValidation;
    }

    @Override
    public void setPerformCrossValidation(boolean performCrossValiation) {
        this.performCrossValidation = performCrossValiation;
    }

    @Override
    public int getNrOfFolds() {
        return nrOfFolds;
    }

    @Override
    public void setNrOfFolds(int nrOfFolds) {
        this.nrOfFolds = nrOfFolds;
    }

    @Override
    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    @Override
    public String getLogLevel() {
        return logLevel;
    }

    @Override
    public AbstractCELA getLearningAlgorithm() {
        return algorithm;
    }

    @Override
    public KnowledgeSource getKnowledgeSource() {
        return knowledgeSource;
    }

    @Override
    public int getNoOfRuns() {
        return noOfRuns;
    }

    @Override
    public void setNoOfRuns(int noOfRuns) {
        this.noOfRuns = noOfRuns;
    }
}

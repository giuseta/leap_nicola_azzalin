/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.bundle;

import pellet.PelletCmdException;

/**
 *
 * @author Giuseppe Cota <giuseta@gmail.com>, Riccardo Zese <riccardo.zese@unife.it>
 */
public class BundleCLIImpl implements BundleCLI {

    BundleController controller = new BundleController();

    public void run(String[] args) throws PelletCmdException{
        
        if (args.length == 0) {
            throw new PelletCmdException("Type 'bundle help' for usage.");
        }

        String arg = args[0];

        if (arg.equals("h") || arg.equals("-h") || arg.equals("help") || arg.equals("--help")) {
            controller.help();
        } else if (arg.equals("--version") || arg.equals("-V")) {
            controller.version();
        } else {
            controller.parseArgs(args);
            controller.run();
            controller.finish();
        }

    }

    public void showHelp() {
        controller.help();
    }
}

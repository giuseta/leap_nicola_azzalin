/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.bundle;

import com.clarkparsia.owlapi.explanation.BlackBoxExplanation;
import com.clarkparsia.owlapi.explanation.GlassBoxExplanation;
import com.clarkparsia.owlapi.explanation.MultipleExplanationGenerator;
import com.clarkparsia.owlapi.explanation.SatisfiabilityConverter;
import com.clarkparsia.owlapi.explanation.TransactionAwareSingleExpGen;
import com.clarkparsia.owlapiv3.OWL;
import com.clarkparsia.owlapiv3.OntologyUtils;
import com.clarkparsia.pellet.owlapiv3.OWLAPILoader;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import net.sf.javabdd.BuDDyFactory;
import org.coode.owlapi.manchesterowlsyntax.ManchesterOWLSyntaxEditorParser;
import org.mindswap.pellet.KBLoader;
import org.mindswap.pellet.KnowledgeBase;
import org.mindswap.pellet.utils.Timer;
import org.mindswap.pellet.utils.progress.ConsoleProgressMonitor;
import org.mindswap.pellet.utils.progress.ProgressMonitor;
import org.semanticweb.owlapi.expression.ParserException;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLProperty;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import pellet.PelletCmdApp;
import pellet.PelletCmdException;
import pellet.PelletCmdOption;
import static pellet.PelletCmdOptionArg.NONE;
import static pellet.PelletCmdOptionArg.REQUIRED;
import pellet.PelletCmdOptions;
import unife.bundle.explanation.BundleGlassBoxExplanation;
import unife.bundle.explanation.BundleHSTExplanationGenerator;
import unife.bundle.monitor.BundleRendererExplanationProgressMonitor;
import unife.bundle.monitor.ScreenRendererTimeExplanationProgressMonitor;
import unife.bundle.monitor.TimeMonitor;
import unife.bundle.utilities.BundleUtilities;

/**
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota <giuseta@gmail.com>
 */
public class BundleDeprecate extends PelletCmdApp {

    private SatisfiabilityConverter converter;
    /**
     * inferences for which there was an error while generating the explanation
     */
    private int errorExpCount = 0;
    private OWLAPILoader loader;
    private int maxExplanations = Integer.MAX_VALUE;
    private int maxTime = 0;
    private boolean useBlackBox = false;
    private ProgressMonitor monitor;
    /**
     * inferences whose explanation contains more than on axiom
     */
    private int multiAxiomExpCount = 0;
    /**
     * inferences with multiple explanations
     */
    private int multipleExpCount = 0;
    private PelletReasoner reasoner;
    private OWLEntity name1;
    private OWLEntity name2;
    private OWLObject name3;
    protected boolean findProbability = true;
    
    //private Map<String, Map<String, List<Double>>> PMap = new HashMap<String, Map<String, List<Double>>>();
    private Map<OWLAxiom, List<Double>> PMap = new HashMap<OWLAxiom, List<Double>>();

    private Double[] prob = new Double[0];
    private String[] axp = new String[0];
    private OWLAxiom[] axi = new OWLAxiom[0];
    private int numAssiomi = 0;

    private BDDFactory bddF;

    public BundleDeprecate() {
        GlassBoxExplanation.setup();
    }

    @Override
    public String getAppId() {
        return "Bundle: Explains one or more inferences in a given ontology including ontology inconsistency";
    }

    @Override
    public String getAppCmd() {
        return "bundle " + getMandatoryOptions() + "[options] <file URI>...\n\n"
                + "The options --unsat, --all-unsat, --inconsistent, --subclass, \n"
                + "--hierarchy, and --instance are mutually exclusive. By default \n "
                + "--inconsistent option is assumed. In the following descriptions \n"
                + "C, D, and i can be URIs or local names.";
    }

    @Override
    public PelletCmdOptions getOptions() {
        PelletCmdOptions options = getGlobalOptions();

        options.add(getIgnoreImportsOption());

        PelletCmdOption option = new PelletCmdOption("unsat");
        option.setType("C");
        option.setDescription("Explain why the given class is unsatisfiable");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("all-unsat");
        option.setDescription("Explain all unsatisfiable classes");
        option.setDefaultValue(false);
        option.setIsMandatory(false);
        option.setArg(NONE);
        options.add(option);

        option = new PelletCmdOption("inconsistent");
        option.setDescription("Explain why the ontology is inconsistent");
        option.setDefaultValue(false);
        option.setIsMandatory(false);
        option.setArg(NONE);
        options.add(option);

        option = new PelletCmdOption("hierarchy");
        option.setDescription("Print all explanations for the class hierarchy");
        option.setDefaultValue(false);
        option.setIsMandatory(false);
        option.setArg(NONE);
        options.add(option);

        option = new PelletCmdOption("subclass");
        option.setDescription("Explain why C is a subclass of D");
        option.setType("C,D");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("instance");
        option.setDescription("Explain why i is an instance of C");
        option.setType("i,C");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("property-value");
        option.setDescription("Explain why s has value o for property p");
        option.setType("s,p,o");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("method");
        option.setShortOption("m");
        option.setType("glass | black");
        option.setDescription("Method that will be used to generate explanations");
        option.setDefaultValue("glass");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("max");
        option.setShortOption("x");
        option.setType("positive integer");
        option.setDescription("Maximum number of generated explanations for each inference");
        option.setDefaultValue(Integer.MAX_VALUE);
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("time");
        option.setShortOption("t");
        option.setDescription("Maximum time allowed for the inference, 0 for unlimited time. Format: [Xh][Ym][Zs][Kms]");
        option.setDefaultValue("0ms");
        option.setIsMandatory(false);
        option.setArg(REQUIRED);
        options.add(option);

        option = new PelletCmdOption("noProb");
        option.setDescription("Disable the computation of the probability");
        option.setDefaultValue(false);
        option.setIsMandatory(false);
        option.setArg(NONE);
        options.add(option);

        option = options.getOption("verbose");
        option.setDescription("Print detailed exceptions and messages about the progress");

        return options;
    }

    @Override
    public void parseArgs(String[] args) {

        // Shift of the arguments because PelletCmdApp reads args array from the position 1.
        String[] argsNew = new String[args.length + 1];
        argsNew[0] = "BUNDLE";
        System.arraycopy(args, 0, argsNew, 1, args.length);
        args = argsNew;
        super.parseArgs(args);

        setMaxExplanations(options.getOption("max").getValueAsNonNegativeInteger());
        setMaxTime(BundleUtilities.convertTimeValue(options.getOption("time").getValueAsString()));
        findProbability = !options.getOption("noProb").getValueAsBoolean();
        if (verbose) {
            if (getMaxExplanations() != Integer.MAX_VALUE) {
                logger.info("Max Explanations: " + getMaxExplanations());
            }
            if (getMaxTime() != 0) {
                logger.info("Max Execution Time: " + options.getOption("time").getValue() + " (" + getMaxTime() + ")");
            }
            if (findProbability != true) {
                logger.info("No probability computation");
            }
        }

        loader = (OWLAPILoader) getLoader("OWLAPIv3");
        getKB(loader, 0);

        if (findProbability) {
            verbose("\n\nLoading probabilities...");
            setPMap();
        }

        converter = new SatisfiabilityConverter(loader.getManager().getOWLDataFactory());
        reasoner = loader.getReasoner();

        loadMethod();
        loadNames();
    }

    protected KnowledgeBase getKB(KBLoader loader, int file) {
        try {
            String[] inputFiles = getInputFiles();

            verbose("There are " + 1 + " input files:");
            verbose(inputFiles[file]);
            

            startTask("loading");
            KnowledgeBase kb = loader.createKB(inputFiles[file]);
            finishTask("loading");

            if (verbose) {
                StringBuilder sb = new StringBuilder();
                sb.append("Classes = " + kb.getAllClasses().size() + ", ");
                sb.append("Properties = " + kb.getProperties().size() + ", ");
                sb.append("Individuals = " + kb.getIndividuals().size());
                verbose("Input size: " + sb);

                verbose("Expressivity: " + kb.getExpressivity());
            }

            return kb;
        } catch (RuntimeException e) {
            throw new PelletCmdException(e);
        }
    }

    private void loadMethod() {
        String method = options.getOption("method").getValueAsString();

        if (method.equalsIgnoreCase("black")) {
            useBlackBox = true;
        } else if (method.equalsIgnoreCase("glass")) {
            useBlackBox = false;
        } else {
            throw new PelletCmdException("Unrecognized method: " + method);
        }
    }

    private void loadNames() {
        PelletCmdOption option;

        name1 = name2 = null;
        name3 = null;

        if ((option = options.getOption("hierarchy")) != null) {
            if (option.getValueAsBoolean()) {
                return;
            }
        }

        if ((option = options.getOption("all-unsat")) != null) {
            if (option.getValueAsBoolean()) {
                name1 = OWL.Nothing;
                return;
            }
        }

        if ((option = options.getOption("inconsistent")) != null) {
            if (option.getValueAsBoolean()) {
                if (useBlackBox) {
                    throw new PelletCmdException("Black box method cannot be used to explain ontology inconsistency");
                }
                name1 = OWL.Thing;
                return;
            }
        }

        if ((option = options.getOption("unsat")) != null) {
            String unsatisfiable = option.getValueAsString();
            if (unsatisfiable != null) {
                name1 = OntologyUtils.findEntity(unsatisfiable, loader.getAllOntologies());

                if (name1 == null) {
                    throw new PelletCmdException("Undefined entity: " + unsatisfiable);
                } else if (!name1.isOWLClass()) {
                    throw new PelletCmdException("Not a defined class: " + unsatisfiable);
                } else if (name1.isTopEntity() && useBlackBox) {
                    throw new PelletCmdException("Black box method cannot be used to explain unsatisfiability of owl:Thing");
                }

                return;
            }
        }

        if ((option = options.getOption("subclass")) != null) {
            String subclass = option.getValueAsString();
            if (subclass != null) {
                String[] names = subclass.split(",");
                if (names.length != 2) {
                    throw new PelletCmdException(
                            "Invalid format for subclass option: " + subclass);
                }

                name1 = OntologyUtils.findEntity(names[0], loader.getAllOntologies());
                name2 = OntologyUtils.findEntity(names[1], loader.getAllOntologies());

                if (name1 == null) {
                    throw new PelletCmdException("Undefined entity: " + names[0]);
                } else if (!name1.isOWLClass()) {
                    throw new PelletCmdException("Not a defined class: " + names[0]);
                }
                if (name2 == null) {
                    throw new PelletCmdException("Undefined entity: " + names[1]);
                } else if (!name2.isOWLClass()) {
                    throw new PelletCmdException("Not a defined class: " + names[1]);
                }
                return;
            }
        }

        if ((option = options.getOption("instance")) != null) {
            String instance = option.getValueAsString();
            if (instance != null) {
                String[] names = instance.split(",");
                if (names.length != 2) {
                    throw new PelletCmdException("Invalid format for instance option: " + instance);
                }

                name1 = OntologyUtils.findEntity(names[0], loader.getAllOntologies());
                name2 = OntologyUtils.findEntity(names[1], loader.getAllOntologies());

                if (name1 == null) {
                    throw new PelletCmdException("Undefined entity: " + names[0]);
                } else if (!name1.isOWLNamedIndividual()) {
                    throw new PelletCmdException("Not a defined individual: " + names[0]);
                }
                if (name2 == null) {
                    throw new PelletCmdException("Undefined entity: " + names[1]);
                } else if (!name2.isOWLClass()) {
                    throw new PelletCmdException("Not a defined class: " + names[1]);
                }

                return;
            }
        }

        if ((option = options.getOption("property-value")) != null) {
            String optionValue = option.getValueAsString();
            if (optionValue != null) {
                String[] names = optionValue.split(",");
                if (names.length != 3) {
                    throw new PelletCmdException("Invalid format for property-value option: " + optionValue);
                }

                name1 = OntologyUtils.findEntity(names[0], loader.getAllOntologies());
                name2 = OntologyUtils.findEntity(names[1], loader.getAllOntologies());

                if (name1 == null) {
                    throw new PelletCmdException("Undefined entity: " + names[0]);
                } else if (!name1.isOWLNamedIndividual()) {
                    throw new PelletCmdException("Not an individual: " + names[0]);
                }
                if (name2 == null) {
                    throw new PelletCmdException("Undefined entity: " + names[1]);
                } else if (!name2.isOWLObjectProperty() && !name2.isOWLDataProperty()) {
                    throw new PelletCmdException("Not a defined property: " + names[1]);
                }
                if (name2.isOWLObjectProperty()) {
                    name3 = OntologyUtils.findEntity(names[2], loader.getAllOntologies());
                    if (name3 == null) {
                        throw new PelletCmdException("Undefined entity: " + names[2]);
                    } else if (!(name3 instanceof OWLIndividual)) {
                        throw new PelletCmdException("Not a defined individual: " + names[2]);
                    }
                } else {
                    ManchesterOWLSyntaxEditorParser parser = new ManchesterOWLSyntaxEditorParser(
                            loader.getManager().getOWLDataFactory(), names[2]);
                    try {
                        name3 = parser.parseConstant();
                    } catch (ParserException e) {
                        throw new PelletCmdException("Not a valid literal: " + names[2]);
                    }
                }

                return;
            }
        }

        // Per default we explain why the ontology is inconsistent
        name1 = OWL.Thing;
        if (useBlackBox) {
            throw new PelletCmdException("Black box method cannot be used to explain ontology inconsistency");
        }

        return;
    }

    private TransactionAwareSingleExpGen getSingleExplanationGenerator() {
        if (useBlackBox) {
            if (options.getOption("inconsistent") != null) {
                if (!options.getOption("inconsistent").getValueAsBoolean()) {
                    return new BlackBoxExplanation(reasoner.getRootOntology(), PelletReasonerFactory.getInstance(), reasoner);
                } else {
                    output("WARNING: black method cannot be used to explain inconsistency. Switching to glass.");
                    //return new BundleGlassBoxExplanation(reasoner);
                    return new BundleGlassBoxExplanation(reasoner);
                }
            } else {
                return new BlackBoxExplanation(reasoner.getRootOntology(), PelletReasonerFactory.getInstance(), reasoner);
            }
        } else {
            return new BundleGlassBoxExplanation(reasoner);
        }
    }

    private void printStatistics() throws OWLException {

        if (!verbose) {
            return;
        }

        Timer timer = timers.getTimer("explain");
        if (timer != null) {
            // TODO changhe with logger????
            verbose("Subclass relations   : " + timer.getCount());
            verbose("Multiple explanations: " + multipleExpCount);
            verbose("Single explanation     ");
            verbose(" with multiple axioms: " + multiAxiomExpCount);
            verbose("Error explaining     : " + errorExpCount);
            verbose("Average time         : " + timer.getAverage() + "ms");
        }
    }

    @Override
    public void finish() {
        finish(false);
    }

    public void finish(boolean clearPMap) {
        super.finish();
//        if (ontologyprob != null)
//            manager.removeOntology(ontologyprob);
//        ontologyprob = null;
        monitor = null;
        loader.clear();
        loader = null;
        reasoner.dispose();
        reasoner = null;
        if (clearPMap) {
            PMap.clear();
        }
        PMap = null;
        prob = null;
        axp = null;
        axi = null;

    }

    public QueryResult computeQuery() {

        QueryResult result = null;

        try {
            if (name1 == null) {
                // Option --hierarchy
                verbose("Explain all the subclass relations in the ontology");
                result = explainClassHierarchy();
            } else if (name2 == null) {
                if (((OWLClassExpression) name1).isOWLNothing()) {
                    //xxx
                    verbose("Explain all the unsatisfiable classes");
                    result = explainUnsatisfiableClasses();
                } else {
                    // Option --inconsistent && --unsat C
                    verbose("Explain unsatisfiability of " + name1);
                    result = explainUnsatisfiableClass((OWLClass) name1);
                }
            } else if (name3 != null) {
                // Option --property-value s,p,o
                verbose("Explain property assertion " + name1 + " and " + name2 + " and " + name3);

                result = explainPropertyValue((OWLIndividual) name1, (OWLProperty<?, ?>) name2, name3);
            } else if (name1.isOWLClass() && name2.isOWLClass()) {
                // Option --subclass C,D
                verbose("Explain subclass relation between " + name1 + " and " + name2);

                result = explainSubClass((OWLClass) name1, (OWLClass) name2);
            } else if (name1.isOWLNamedIndividual() && name2.isOWLClass()) {
                // Option --instance i,C
                verbose("Explain instance relation between " + name1 + " and " + name2);

                result = explainInstance((OWLIndividual) name1, (OWLClass) name2);
            }
            //printStatistics();

        } catch (OWLException e) {
            if (findProbability && getBddF() != null) {
                getBddF().done();
            }
            throw new RuntimeException(e);
        }
        computeProbability(result);
        return result;
    }

    public QueryResult computeQuery(OWLAxiom query) throws OWLException {
        QueryResult result = explainAxiom(query);
        computeProbability(result);
        return result;
    }

    private QueryResult explainClassHierarchy() throws OWLException {
        Set<OWLClass> visited = new HashSet<OWLClass>();

        reasoner.flush();

        startTask("Classification");
        reasoner.getKB().classify();
        finishTask("Classification");

        startTask("Realization");
        reasoner.getKB().realize();
        finishTask("Realization");

        monitor = new ConsoleProgressMonitor();
        monitor.setProgressTitle("Explaining");
        monitor.setProgressLength(reasoner.getRootOntology().getClassesInSignature().size());
        monitor.taskStarted();

        Node<OWLClass> bottoms = reasoner.getEquivalentClasses(OWL.Nothing);
        QueryResult topsResults = explainClassHierarchy(OWL.Nothing, bottoms, visited);

        Node<OWLClass> tops = reasoner.getEquivalentClasses(OWL.Thing);
        QueryResult bottomsResults = explainClassHierarchy(OWL.Thing, tops, visited);

        monitor.taskFinished();

        topsResults.merge(bottomsResults);
        bottomsResults = null; //pulizia
        return topsResults;

    }

    private QueryResult explainClassHierarchy(OWLClass cls, Node<OWLClass> eqClasses, Set<OWLClass> visited)
            throws OWLException {

        QueryResult results = new QueryResult();

        if (visited.contains(cls)) {
            return null;
        }

        visited.add(cls);
        visited.addAll(eqClasses.getEntities());

        for (OWLClass eqClass : eqClasses) {
            monitor.incrementProgress();

            results.merge(explainEquivalentClass(cls, eqClass));
        }

        for (OWLNamedIndividual ind : reasoner.getInstances(cls, true).getFlattened()) {
            results.merge(explainInstance(ind, cls));
        }

        NodeSet<OWLClass> subClasses = reasoner.getSubClasses(cls, true);
        Map<OWLClass, Node<OWLClass>> subClassEqs = new HashMap<OWLClass, Node<OWLClass>>();
        for (Node<OWLClass> equivalenceSet : subClasses) {
            if (equivalenceSet.isBottomNode()) {
                continue;
            }

            OWLClass subClass = equivalenceSet.getRepresentativeElement();
            subClassEqs.put(subClass, equivalenceSet);
            results.merge(explainSubClass(subClass, cls));
        }

        for (Map.Entry<OWLClass, Node<OWLClass>> entry : subClassEqs.entrySet()) {
            results.merge(explainClassHierarchy(entry.getKey(), entry.getValue(), visited));
        }

        return results;
    }

    private QueryResult explainUnsatisfiableClasses() throws OWLException {

        QueryResult results = new QueryResult();

        for (OWLClass cls : reasoner.getEquivalentClasses(OWL.Nothing)) {
            if (cls.isOWLNothing()) {
                continue;
            }

            results.merge(explainUnsatisfiableClass(cls));
        }

        return results;
    }

    private QueryResult explainUnsatisfiableClass(OWLClass owlClass) throws OWLException {
        return explainSubClass(owlClass, OWL.Nothing);
    }

    // In the following method(s) we intentionally do not use OWLPropertyExpression<?,?>
    // because of a bug in some Sun's implementation of javac
    // http://bugs.sun.com/view_bug.do?bug_id=6548436
    // Since lack of generic type generates a warning, we suppress it
    @SuppressWarnings("unchecked")
    public QueryResult explainPropertyValue(OWLIndividual s, OWLProperty p, OWLObject o) throws OWLException {
        if (p.isOWLObjectProperty()) {
            return explainAxiom(OWL.propertyAssertion(s, (OWLObjectProperty) p, (OWLIndividual) o));
        } else {
            return explainAxiom(OWL.propertyAssertion(s, (OWLDataProperty) p, (OWLLiteral) o));
        }
    }

    private QueryResult explainSubClass(OWLClass sub, OWLClass sup) throws OWLException {
        PelletCmdOption option;
        if (sub.equals(sup)) {
            return null;
        }

        if (sub.isOWLNothing()) {
            return null;
        }

        if (sup.isOWLThing()) {
            return null;
        }

        OWLSubClassOfAxiom axiom = OWL.subClassOf(sub, sup);
        return explainAxiom(axiom);

    }

    private QueryResult explainInstance(OWLIndividual owlIndividual, OWLClass owlClass) throws OWLException {
        if (owlClass.isOWLThing()) {
            return null;
        }

        OWLAxiom axiom = OWL.classAssertion(owlIndividual, owlClass);

        return explainAxiom(axiom);
    }

    public QueryResult explainEquivalentClass(OWLClass c1, OWLClass c2) throws OWLException {
        if (c1.equals(c2)) {
            return null;
        }

        OWLAxiom axiom = OWL.equivalentClasses(c1, c2);

        return explainAxiom(axiom);
    }

    private QueryResult explainAxiom(OWLAxiom axiom) throws OWLException {

        MultipleExplanationGenerator expGen = new BundleHSTExplanationGenerator(getSingleExplanationGenerator());
        //MultipleExplanationGenerator expGen = new HSTExplanationGenerator(getSingleExplanationGenerator());
        BundleRendererExplanationProgressMonitor rendererMonitor = new ScreenRendererTimeExplanationProgressMonitor(axiom);
        expGen.setProgressMonitor(rendererMonitor);

        OWLClassExpression unsatClass = converter.convert(axiom);

        QueryResult result = new QueryResult();

        Timer timer = timers.startTimer("explain");
        ((TimeMonitor) rendererMonitor).setParamAndStart(getMaxTime());

        Set<Set<OWLAxiom>> explanations = expGen.getExplanations(unsatClass, getMaxExplanations());

        rendererMonitor.stopMonitoring();
        timer.stop();

        if (explanations.isEmpty()) {
            rendererMonitor.foundNoExplanations();
        }

        if (timer.getCount() % 10 == 0) {
            // printStatistics();
        }

        int expSize = explanations.size();
        if (expSize == 0) {
            errorExpCount++;
        } else if (expSize == 1) {
            if (explanations.iterator().next().size() > 1) {
                multiAxiomExpCount++;
            }
        } else {
            multipleExpCount++;
        }

        result.setExplanations(explanations);

        return result;
    }

    private void setPMap() throws NumberFormatException {

        OWLAPILoader loaderprob = null;
        OWLOntology ontologyprob;

        if (getInputFiles().length == 2) {
            loaderprob = (OWLAPILoader) getLoader("OWLAPIv3");
            getKB(loaderprob, 1);
            ontologyprob = loaderprob.getOntology();
        } else {
            ontologyprob = loader.getOntology();
        }

        for (OWLAxiom axiom : ontologyprob.getAxioms())//prob
        {
            //HashMap<String, List<Double>> varProb = new HashMap<String, List<Double>>();
            List<Double> listDoubleTemp = new ArrayList<Double>();
            String axiomName = BundleUtilities.getManchesterSyntaxString(axiom);

            for (OWLAnnotation annotation : axiom.getAnnotations()) {

                if (annotation.getValue() != null) {

                    if (annotation.getProperty().toString().contains("probability")) {

                        System.out.print(axiomName);

                        String annotazione = annotation.getValue().toString();
                        String annotazione1 = annotazione.replaceAll("\"", "");
                        if (annotazione1.contains("^^")) {
                            annotazione1 = annotazione1.substring(0, annotazione1.indexOf("^^"));
                        }

                        Double annProbability = Double.valueOf(annotazione1);
                        listDoubleTemp.add(annProbability);
                        System.out.println(" => epistemic (" + annProbability + ")");

                    }
                }

            }
            if (listDoubleTemp.size() > 0) {
                if (PMap.containsKey(axiom)) {
                    List<Double> varProbTemp = PMap.get(axiom);
                    varProbTemp.addAll(listDoubleTemp);

                } else {
                    PMap.put(axiom.getAxiomWithoutAnnotations(), listDoubleTemp);
                }
            }

        }

        ontologyprob = null;

        if (getInputFiles().length == 2) {
            loaderprob.clear();
        }

    }
    
    public void setPMap(Map<OWLAxiom, List<Double>> PMap) {
        this.PMap = PMap;
    }

    private void computeProbability(QueryResult explanations) {
        if (findProbability) {
            Timer timerBDD = timers.startTimer("BDDCalc");

            // VarAxAnn composed by two (three) arrays:
            //      prob    (probability of axioms)
            //      axi     (OWLAxiom)
            //     (axp     (translations of axioms into strings))
            setBddF(BuDDyFactory.init(100, 10000));

            BDD bdd = getBDD(explanations.getExplanations());

            Double pq = probabilityOfBDD(bdd, new HashMap<BDD, Double>());

            timerBDD.stop();

            // ?? MAYBE
            if ((pq < 1.0)
                    && (//BlockedIndividualSet.isApproximate() || 
                    getMaxExplanations() == explanations.getNumberOfExplanations())) {
                System.out.println("WARNING! The value of the probability may be a lower bound.");
            }

            explanations.setBDD(bdd);
            explanations.setQueryProbability(pq);
            explanations.setProbAxioms(Arrays.asList(axi));
            explanations.setProbOfAxioms(Arrays.asList(prob));
        }
    }

    private BDD getBDD(Set<Set<OWLAxiom>> explanations) {
        BDD bdd = getBddF().zero();

        //for all explanation (minA) in explanations (CS)
        for (Iterator<Set<OWLAxiom>> it = explanations.iterator(); it.hasNext();) {
            BDD bdde = getBddF().one();
            Set<OWLAxiom> s = it.next();
            //for all axiom in explanation
            for (Iterator<OWLAxiom> it1 = s.iterator(); it1.hasNext();) {
                boolean stop = false;
                BDD bdda;
                numAssiomi++;
                OWLAxiom s1 = it1.next();
                String s1name = BundleUtilities.getManchesterSyntaxString(s1);
                //System.out.println(stringWriter.toString());

                // TO CHECK AND FIX
                if (s1name.startsWith("Transitive")) {
                    String[] splits1name = s1name.split(" ", 2);
                    s1name = splits1name[1] + " type " + splits1name[0] + "Property";
                }

                if (!PMap.containsKey(s1)) {
                    bdda = getBddF().one();
                } else {
                    // Creazione e inizializzazione di Res
                    List<Double> probsInstSet = PMap.get(s1);

                    bdda = getBddF().zero();

                    if (stop) {
                        bdde.andWith(bdda);
                        //printCalculateBDD(s, s1, key, axiomVar);
                        break;
                    }

                    int i;
                    boolean found = false;
                    for (i = 0; i < axi.length; i++) {
                        if (axi[i].equals(s1)) {
                            found = true;
                            break;
                        }
                    }

                    //for (Double ithProb : probsInstSet) {
                    if (!found) {
                        int oldLength = axi.length;
                        String[] temp = new String[oldLength + probsInstSet.size()];  // The new array.
                        Double[] tempd = new Double[oldLength + probsInstSet.size()];
                        OWLAxiom[] tempai = new OWLAxiom[oldLength + probsInstSet.size()];
                        System.arraycopy(axp, 0, temp, 0, oldLength);
                        System.arraycopy(prob, 0, tempd, 0, oldLength);
                        System.arraycopy(axi, 0, tempai, 0, oldLength);
                        axp = temp;  // Set playerList to refer to new array.
                        prob = tempd;
                        axi = tempai;
                        for (int iForProbs = 0; iForProbs < probsInstSet.size(); iForProbs++) {
                            axp[oldLength + iForProbs] = s1name;
                            prob[oldLength + iForProbs] = probsInstSet.get(iForProbs);
                            axi[oldLength + iForProbs] = s1;
                        }
                    }
//                      

                    getBddF().setVarNum(getBddF().varNum() + probsInstSet.size());
                    for (int iForProbs = 0; iForProbs < probsInstSet.size(); iForProbs++) {
                        BDD b = getBddF().ithVar(i + iForProbs);
                        bdda.orWith(b);
                    }
                }

                bdde.andWith(bdda);
            }

            bdd.orWith(bdde);
        }
        return bdd;
    }

    /*
     ************************************************************************
     * 
     * DA METTERE QUA?????
     * 
     ************************************************************************
     */
    public Double probabilityOfBDD(BDD node, Map<BDD, Double> nodes) {

        if (node.isOne()) {
            return 1.0;
        } else if (node.isZero()) {
            return 0.0;
        } else {

            Double value_p = getValue(node, nodes);
            if (value_p != null) {
                return value_p;
            } else {
                Double p = prob[node.var()];
                Double pt = p * probabilityOfBDD(node.high(), nodes) + (1 - p) * probabilityOfBDD(node.low(), nodes);
                add_node(node, pt, nodes);
                return pt;
            }

        }

        //return 0.0;
    }

    private Double getValue(BDD node, Map<BDD, Double> nodes) {
        if (nodes.containsKey(node)) {
            return nodes.get(node);
        } else {
            return null;
        }
    }

    private void add_node(BDD node, Double pt, Map<BDD, Double> nodes) {

        nodes.put(node, pt);

    }

    /**
     * **********************************************************************
     */
    // TO FIX
    private void printCalculateBDD(Set<OWLAxiom> exp, OWLAxiom ax, String probType, String axType) {
        /*
         System.out.println("\n\nSPIEGAZIONE ANNULLATA!\n");
         for (OWLAxiom axiom : exp){
            
         String s1name = Utilities.getManchesterSyntaxString(axiom);
         System.out.println("    " + s1name);
         if (axiom.getAxiomType().toString().equals("SubClassOf") ||
         axiom.getAxiomType().toString().equals("TransitiveObjectProperty") ||
         axiom.getAxiomType().toString().equals("SubObjectPropertyOf") ||
         axiom.getAxiomType().toString().equals("SubPropertyChain") ||
         axiom.getAxiomType().toString().equals("SubDataPropertyOf")){
         System.out.println("        "+ ((OWLIndAxiom)axiom).getSubjName());
         if (((OWLIndAxiom)axiom).equalsStrict(ax)){
         System.out.println("     <= CAUSATO DA QUESTO ASSIOMA\n"+
         "           Prob\t :  Assioma\n" +
         "           " + probType + "\t :    " + axType);
         }
         } else if (axiom.equals(ax)){
         System.out.println("     <= CAUSATO DA QUESTO ASSIOMA\n"+
         "           Prob\t :  Assioma\n" +
         "           " + probType + "\t :    " + axType);
         }
            
         }
         */
        System.out.println("Not yet implemented!");
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the maxExplanations
     */
    public int getMaxExplanations() {
        return maxExplanations;
    }

    /**
     * @param maxExplanations the maxExplanations to set
     */
    public void setMaxExplanations(int maxExplanations) {
        this.maxExplanations = maxExplanations;
    }

    /**
     * @return the maximum time for execution
     */
    public int getMaxTime() {
        return maxTime;
    }

    /**
     * @param maxTime the maximum time for execution to set
     */
    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }
    
    /**
     * @param maxTime the string of the maximum time for execution to set
     */
    public void setMaxTime(String maxTime) {
        this.maxTime = BundleUtilities.convertTimeValue(maxTime);
    }

    /**
     * @return the bdd factory
     */
    public BDDFactory getBddF() {
        return bddF;
    }

    /**
     * @param bddF the bdd factory to set
     */
    public void setBddF(BDDFactory bddF) {
        this.bddF = bddF;
    }

}

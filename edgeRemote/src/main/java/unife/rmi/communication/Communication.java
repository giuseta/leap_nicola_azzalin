/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.rmi.communication;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.rmi.RemoteException;

/**
 * This is an class used for RMI communication.
 * @author Giuseppe Cota <giuseta@gmail.com>, Riccardo Zese <riccardo.zese@unife.it>
 */
public class Communication {
    
//    public static OutputStream getOutputStream() throws RemoteException {
//        return new RMISerializableOutputStream(new RMIOutputStreamImpl(new PipedOutputStream()));
//    }
//    
//    public static InputStream getInputStream() throws RemoteException, IOException {
//        return new RMISerializableInputStream(new RMIInputStreamImpl(new PipedInputStream()));
//    }
    
    public static OutputStream getOutputStream(File f) throws FileNotFoundException, RemoteException, IOException {
        String path = f.getCanonicalPath();
        String filename = path.substring(path.lastIndexOf("/"));
        String directory = "tmp/";
        new File(directory).mkdir();
        return new RMISerializableOutputStream(new RMIOutputStreamImpl(new FileOutputStream(new File(directory + filename))));
    }
    
    public static InputStream getInputStream(File f) throws IOException {
        return new RMISerializableInputStream(new RMIInputStreamImpl(new FileInputStream(f)));
    }
    
//    public static InputStream getInputStream(PipedInputStream inPipe) throws IOException {
//        return new RMISerializableInputStream(new RMIInputStreamImpl(inPipe));
//    }
//    
//    public static OutputStream getOutputStream(PipedOutputStream outPipe) throws RemoteException {
//        return new RMISerializableOutputStream(new RMIOutputStreamImpl(outPipe));
//    }
}

/**
 *  This file is part of LEAP.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  LEAP is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  LEAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package unife.utilities;

//import com.clarkparsia.owlapi.explanation.io.manchester.ManchesterSyntaxObjectRenderer;
import com.clarkparsia.owlapi.explanation.io.manchester.TextBlockWriter;
import uk.ac.manchester.cs.owl.owlapi.mansyntaxrenderer.ManchesterOWLSyntaxObjectRenderer;

import java.io.StringWriter;
import java.util.HashSet;
import java.util.Set;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;

/**
 * Class containing some useful methods.
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota <giuseta@gmail.com>
 */
public class Utility {

    /**
     * This method converts an OWL object to Manchester syntax.
     *
     * @param owlObject
     * @return
     */
    public static String getManchesterSyntaxString(OWLObject owlObject) {
        StringWriter stringWriter = new StringWriter();
        TextBlockWriter printWriter = new TextBlockWriter(stringWriter);
        ManchesterOWLSyntaxObjectRenderer manch = new ManchesterOWLSyntaxObjectRenderer(printWriter, new SimpleShortFormProvider());
        owlObject.accept(manch);
        return stringWriter.toString();
    }

    /**
     * It returns a copy of a given OWL ontology
     *
     * @param manager
     * @param ontology
     * @return
     * @throws OWLOntologyCreationException
     */
    public static OWLOntology copyOntology(OWLOntologyManager manager, OWLOntology ontology) throws OWLOntologyCreationException {
        OWLOntology ontologyCopy = null;
        ontologyCopy = manager.createOntology();
        manager.addAxioms(ontologyCopy, ontology.getAxioms());
        return ontologyCopy;
    }

    /**
     * It returns an ontology containing only the certain axioms of a given
     * ontology.
     *
     * @param manager
     * @param ontology
     * @return
     * @throws OWLOntologyCreationException
     */
    public static OWLOntology copyCertainOntology(OWLOntologyManager manager, OWLOntology ontology) throws OWLOntologyCreationException {
        OWLOntology ontologyCopy = Utility.copyOntology(manager, ontology);
        // probabilistic axioms to remove
        Set<OWLAxiom> probAxioms = Utility.getProbabilisticAxioms(ontologyCopy);
        manager.removeAxioms(ontologyCopy, probAxioms);
        return ontologyCopy;
    }

    /**
     * It returns the set of the probabilistic axioms.
     * 
     * @param ontology
     * @return 
     */
    public static Set<OWLAxiom> getProbabilisticAxioms(OWLOntology ontology) {
        Set<OWLAxiom> probAxioms = new HashSet<OWLAxiom>();
        for (OWLAxiom axiom : ontology.getAxioms()) {
            for (OWLAnnotation annotation : axiom.getAnnotations()) {
                if (annotation.getValue() != null) {
                    if (annotation.getProperty().toString().contains("probability")) {
                        probAxioms.add(axiom);
                    }
                }
            }

        }
        return probAxioms;
    }
}
